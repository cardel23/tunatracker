package com.pwk.track.nfc.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SuiteEntity extends SugarRecord<SuiteEntity> {

    @Expose
    @SerializedName("type")
    protected String entityName;

    @Expose
    @SerializedName("id")
    protected String entityId;

    @Expose
    @SerializedName("date_entered")
    protected String dateEntered;

    @Expose
    @SerializedName("date_modified")
    protected String dateModified;

    @Expose
    @SerializedName("created_by")
    protected String createdBy;

    @Expose
    @SerializedName("created_by_name")
    protected String createdByName;

    @Expose
    @SerializedName("modified_user_id")
    protected String updatedBy;

    @Expose
    @SerializedName("modified_by_name")
    protected String updatedByName;

    @Expose
    @SerializedName("description")
    protected String description;

    @Expose
    @SerializedName("name")
    protected String name;


    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getDateEntered() {
        return dateEntered;
    }

    public void setDateEntered(String dateEntered) {
        this.dateEntered = dateEntered;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedByName() {
        return updatedByName;
    }

    public void setUpdatedByName(String updatedByName) {
        this.updatedByName = updatedByName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConvertedDate(String dateString){
        if(dateString.contains("+")) {
            dateString = (dateString.replace(dateString.substring(dateString.indexOf("+")), ""));
        }
        SimpleDateFormat parse = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date parsedate = parse.parse(dateString);
            SimpleDateFormat sdf = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss");
            Date date = new Date(parsedate.getTime());
            this.save();
            return sdf.format(date);
        } catch (ParseException e) {
            return dateString;
        }
    }
}
