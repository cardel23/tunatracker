package com.pwk.track.nfc.api;

import com.pwk.track.nfc.data.CRMKey;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Created by pwk04 on 05-28-19.
 */

public class CRMKeySerializer implements JsonSerializer<CRMKey> {
    @Override
    public JsonElement serialize(CRMKey src, Type typeOfSrc, JsonSerializationContext context) {
        Gson gson =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        JsonParser parser = new JsonParser();
        JsonObject invoiceJson = parser.parse(gson.toJson(src, CRMKey.class)).getAsJsonObject();
        return invoiceJson;
    }
}
