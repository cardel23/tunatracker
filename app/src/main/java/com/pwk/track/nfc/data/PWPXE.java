package com.pwk.track.nfc.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PWPXE extends SuiteEntity {

    @Expose
    @SerializedName("codigo_sensor_c")
    private String sensorCode;

    @Expose
    @SerializedName("codigo_envio_c")
    private String shipmentCode;

    @Expose
    @SerializedName("producto_id_c")
    private String productId;

    public String getSensorCode() {
        return sensorCode;
    }

    public void setSensorCode(String sensorCode) {
        this.sensorCode = sensorCode;
    }

    public String getShipmentCode() {
        return shipmentCode;
    }

    public void setShipmentCode(String shipmentCode) {
        this.shipmentCode = shipmentCode;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
