package com.pwk.track.nfc.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sensor extends SuiteEntity {

    @Expose
    @SerializedName("codigo_sensor_c")
    private String sensorCode;

    public String getSensorCode() {
        return sensorCode;
    }

    public void setSensorCode(String sensorCode) {
        this.sensorCode = sensorCode;
    }
}
