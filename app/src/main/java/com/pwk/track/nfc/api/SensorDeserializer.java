package com.pwk.track.nfc.api;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.pwk.track.nfc.data.Sensor;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SensorDeserializer implements JsonDeserializer<List<Sensor>> {

    public static final String TAG = SensorDeserializer.class.getSimpleName();


    @Override
    public List<Sensor> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        try {
            JsonElement jsonResponse = new JsonParser().parse(json.toString()).getAsJsonObject();
            JsonArray data = jsonResponse.getAsJsonObject().getAsJsonArray("data").getAsJsonArray();
            List<Sensor> elementsArray;
            try {
                elementsArray = new ArrayList<>();
                try {
                    for (JsonElement jsonElement : data) {
                        try {
                            JsonElement attributes = jsonElement.getAsJsonObject().get("attributes").getAsJsonObject();
                            Sensor element = new Sensor();
                            element.setName(attributes.getAsJsonObject().get("name").getAsString());
                            element.setDescription(attributes.getAsJsonObject().get("description").getAsString());
                            element.setSensorCode(attributes.getAsJsonObject().get("codigo_sensor_c").getAsString());
                            elementsArray.add(element);
                        } catch (JsonSyntaxException e2) {
                            throw e2;
                        }
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                    throw new JsonParseException(e1.getMessage());
                }
                return elementsArray;
            } catch (Exception e) {

            }
        } catch (JsonParseException e) {
            Log.e(TAG, e.getMessage(), e);
            throw e;
        }
        return null;
    }
}
