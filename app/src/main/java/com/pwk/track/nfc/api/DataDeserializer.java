package com.pwk.track.nfc.api;

import android.util.Log;

import com.pwk.track.nfc.data.NFCData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pwk04 on 05-16-19.
 */

public class DataDeserializer implements JsonDeserializer<List<NFCData>> {
    public static final String TAG = "OBListDeserializer";
    public Class<NFCData> classType;
    public List<NFCData> deserialize(JsonElement json, Type type,
                               JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new GsonBuilder().
                excludeFieldsWithoutExposeAnnotation()
//					.setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK")
                .create();
        try {
            JsonArray jsonResponse = new JsonParser().parse(json.toString()).getAsJsonArray();
            for (JsonElement e : jsonResponse) {

            }
            JsonArray data = jsonResponse.getAsJsonObject().getAsJsonArray("data").getAsJsonArray();
            List<NFCData> elementsArray;
            try {
                elementsArray = new ArrayList<NFCData>();
                try {
                    for (JsonElement jsonElement : data) {
                        String entityName = jsonElement.getAsJsonObject().get("_entityName").getAsString();
                        try {
                            NFCData element = context.deserialize(jsonElement, Class.forName(NFCData.class.getName()));
                            elementsArray.add(element);
                        } catch (JsonSyntaxException e2) {
                            NFCData element = new NFCData();

                            e2.printStackTrace();
                        }

                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                    throw new JsonParseException(e1.getMessage());
                }
                return elementsArray;
            } catch (Exception e) {

            }
        } catch (JsonParseException e) {
            Log.e(TAG, e.getMessage(), e);
            throw e;
        }
        return null;
    }

}