package com.pwk.track.nfc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;
import android.widget.TextView;

import com.pwk.track.nfc.adapter.AOSProductAdapter;
import com.pwk.track.nfc.data.AOSProduct;

import java.util.List;


/**
 * @author Carlos Delgadillo
 */
public class TrackActivity extends AppCompatActivity {

    private List<AOSProduct> products;
    private App app;
    private RecyclerView recycler;
    private AOSProductAdapter dataAdapter;
    private RecyclerView.LayoutManager lManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_track);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        App app = (App) getApplicationContext();
//        ListView list = (ListView) findViewById(R.id.listView2);
////        List<NFCPathRecord> records = app.getCurrent().getRecords();
////        NFCRecordAdapter adapter = new NFCRecordAdapter(TrackActivity.this, records);
//        AOSProductAdapter adapter = new AOSProductAdapter(TrackActivity.this, app.getCurrent().getProductList());
////        list.setAdapter(adapter);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        app = (App) getApplicationContext();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Detalle de productos");
        TextView envio, sensor;
        envio = (TextView) findViewById(R.id.envio);
        sensor = (TextView) findViewById(R.id.sensor);
        envio.setText("Envío: " + app.getCurrent().getShipCode());
        sensor.setText("Sensor: " + app.getCurrent().getCallsign());
        recycler = (RecyclerView) findViewById(R.id.recycler2);
        dataAdapter = new AOSProductAdapter(this, app.getCurrent().getProductList());
        lManager = new LinearLayoutManager(this);
//        dataAdapter.setHasStableIds(true);
        recycler.setAdapter(dataAdapter);
        recycler.setLayoutManager(lManager);
//        dataAdapter.setHasStableIds(true);

//        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(6);
    }
}
