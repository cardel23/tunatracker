package com.pwk.track.nfc.data;

import java.util.HashMap;

public class Constants {

    private static final HashMap<String, String> ENTITY_NAME_MAP = new HashMap<>();
    private static final String SENSOR = "PW_Sensores";
    private static final String PRODXSHIP = "PW_ProductosXEnvio";
    private static final String PRODUCT = "AOS_Products";
    private static final String SHIPMENT= "Meeting";

    public static final String PRODUCT_IMAGES_DIRNAME = "products";

    public static String getClassNames(String entityName){
//    if(ENTITY_NAME_MAP == null){
//      ENTITY_NAME_MAP = new HashMap<String,String>();

        /** Agregar los maps con los nombres de entidades y su clase respectiva **/
        ENTITY_NAME_MAP.put(SENSOR, "com.pwk.track.nfc.data.Sensor");
        ENTITY_NAME_MAP.put(PRODXSHIP, "com.pwk.track.nfc.data.PWPXE");
        ENTITY_NAME_MAP.put(PRODUCT,"com.pwk.track.nfc.data.AOSProduct");
        ENTITY_NAME_MAP.put(SHIPMENT,"com.pwk.track.nfc.data.Shipment");
//        ENTITY_NAME_MAP.put()
//      /** Llenar la lista con los maps **/
//      ENTITY_NAME_MAP.add(ENTITY_NAME_MAP);
//    }

//    for(HashMap<String, String> h : ENTITY_NAME_MAP ){

        if(ENTITY_NAME_MAP.containsKey(entityName))
            return ENTITY_NAME_MAP.get(entityName);
//    }
        return null;
    }
}
