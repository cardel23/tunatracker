package com.pwk.track.nfc.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class AOSProduct extends SuiteEntity {

    @Expose
    @SerializedName("part_number")
    private String partNumber;

    @Expose
    @SerializedName("maincode")
    private String mainCode;

    @Expose
    @SerializedName("product_image")
    private String productImageHtml;

    private String imageBufferedString;

    public String getPartNumber() {
        if(partNumber == null)
            return "--";
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getMainCode() {
        if(mainCode == null)
            return "--";
        return mainCode;
    }

    public void setMainCode(String mainCode) {
        this.mainCode = mainCode;
    }

    public String getProductImageHtml() {
        return productImageHtml;
    }

    public void setProductImageHtml(String productImageHtml) {
        this.productImageHtml = productImageHtml;
    }

    public String getImageBufferedString() {
        return imageBufferedString;
    }

    public void setImageBufferedString(String imageBufferedString) {
        this.imageBufferedString = imageBufferedString;
    }
}
