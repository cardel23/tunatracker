package com.pwk.track.nfc.api;

import com.pwk.track.nfc.data.CRMNote;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Created by pwk04 on 05-28-19.
 */

public class CRMNoteSerializer implements JsonSerializer<CRMNote> {
    @Override
    public JsonElement serialize(CRMNote src, Type typeOfSrc, JsonSerializationContext context) {
        Gson gson =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setLenient().create();
        JsonParser parser = new JsonParser();
        JsonObject objectWrapper = new JsonObject();
        JsonObject invoiceJson = parser.parse(gson.toJson(src, CRMNote.class)).getAsJsonObject();
//        goodReceiptJson.add("businessPartner",
//                parser.parse("{\"id\": " + goodReceiptJson.get("businessPartner").getAsString() + "}"));
//        goodReceiptJson.add("partnerAddress",
//                parser.parse("{\"id\": " + goodReceiptJson.get("partnerAddress").getAsString() + "}"));
//        goodReceiptJson.add("documentType",
//                parser.parse("{\"id\": "+ goodReceiptJson.get("documentType").getAsString() + "}"));

        objectWrapper.add("data", invoiceJson);
        return objectWrapper;
    }
}
