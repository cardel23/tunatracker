package com.pwk.track.nfc.data;

import com.orm.SugarRecord;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by pwk04 on 05-16-19.
 * @author Carlos Delgadillo
 */

public class NFCPathRecord extends SugarRecord<NFCPathRecord> {

    private long time;
    private double latitude;
    private double longitude;
    private double altitude;
    private double trueTrack;
    private boolean onGround;
    private double temp;
    private long dataId;

    public NFCPathRecord(){
        setTemp(Math.random()*(((-35)-(-45))+1)+(-45));
    }


    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getTrueTrack() {
        return trueTrack;
    }

    public void setTrueTrack(double trueTrack) {
        this.trueTrack = trueTrack;
    }

    public boolean isOnGround() {
        return onGround;
    }

    public void setOnGround(boolean onGround) {
        this.onGround = onGround;
    }

    public Date getDate(){
        Date date = new Date();
        date.setTime(getTime());
        return date;
    }

    public String getStringDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("EE, dd/MM/yyyy, HH:mm");
        Date date = new Date();
        date.setTime(getTime());
        return sdf.format(date);
    }

    public void setTemp(double temp) {
        BigDecimal formatNumber = new BigDecimal(temp);
        formatNumber = formatNumber.setScale(2, RoundingMode.DOWN);
        this.temp = formatNumber.doubleValue();
    }

    public double getTemp(){
        return temp;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Fecha: "+getStringDate());
        sb.append("\n");
        sb.append("Latitud: "+getLatitude()+"N");
        sb.append("\n");
        sb.append("Longitud: "+getLongitude()+"E");
        sb.append("\n");
        sb.append("Altitud: "+getAltitude()+"m");
        sb.append("\n");
        sb.append("Temperatura: "+getTemp()+"ºCº");
        sb.append("\n");
        return sb.toString();
    }

    public long getDataId() {
        return dataId;
    }

    public void setDataId(long dataId) {
        this.dataId = dataId;
    }
}
