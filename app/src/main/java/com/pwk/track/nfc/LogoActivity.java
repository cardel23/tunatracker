package com.pwk.track.nfc;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.pwk.track.nfc.data.NFCData;

import java.util.Timer;
import java.util.TimerTask;

public class LogoActivity extends DemoBase {

    Timer timer;
    TimerTask task;
    final int TIME = 1200;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int count = NFCData.findWithQuery(NFCData.class,"SELECT * FROM NFC_DATA").size();
        if(count>0){
            start();
        }
        else {
            setContentView(R.layout.activity_logo);
            timer = new Timer();
            task = new TimerTask() {
                @Override
                public void run() {
                    if(checkPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,2))
                        start();
                }
            };
            timer.schedule(task, TIME);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 2:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    start();
                }

                // other 'case' lines to check for other
                // permissions this app might request
        }
    }

    void start(){
        Intent intentLogin = new Intent(LogoActivity.this,
                NFCActivity.class);
        System.out.print("Write mode "+App.getContext().isWriteModeEnabled());
        App.getContext().appendLog("Iniciando aplicacion");
        startActivity(intentLogin);
        finish();
    }
}
