package com.pwk.track.nfc.adapter;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.pwk.track.nfc.App;
import com.pwk.track.nfc.R;
import com.pwk.track.nfc.data.NFCData;
import com.pwk.track.nfc.data.Shipment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pwk04 on 05-21-19.
 * @author Carlos Delgadillo
 */

public class NFCDataAdapter extends RecyclerView.Adapter<NFCDataAdapter.NFCViewHolder>{
    private List<NFCData> items;
    private App app;
    public AdapterView.OnClickListener listener;
    private SparseBooleanArray mSelectedItemsIds = new SparseBooleanArray();
    private AdapterView.OnLongClickListener onLongClickListener;

    public class NFCViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
//        public ImageView imagen;
        public TextView producto;
        public TextView ref;
        public TextView salida;
        public TextView entrada;
        public TextView pesoc;
        public TextView fechac;
        public TextView pesom;
        public TextView fecham;

        public NFCViewHolder(View v) {
            super(v);
            producto = (TextView) v.findViewById(R.id.producto);
            ref = (TextView) v.findViewById(R.id.ref);
            salida = (TextView) v.findViewById(R.id.salida);
            entrada = (TextView) v.findViewById(R.id.entrada);
            pesoc = (TextView) v.findViewById(R.id.pesoc);
            fechac = (TextView) v.findViewById(R.id.fechac);
            pesom = (TextView) v.findViewById(R.id.pesom);
            fecham = (TextView) v.findViewById(R.id.fecham);
//            imagen = (ImageView) v.findViewById(R.id.image);
            itemView.setTag(this);
            if(listener != null)
                itemView.setOnClickListener(listener);
            if(onLongClickListener != null)
                itemView.setOnLongClickListener(onLongClickListener);
        }
    }

    public NFCDataAdapter(List<NFCData> items, App app) {
        this.items = items;
        this.app = app;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(NFCData data){
        int index = checkExists(data);
        if(index == -1)
            items.add(data);
        else {
            items.remove(index);
            items.add(index, data);
        }
    }

    @Override
    public NFCViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_nfc, viewGroup, false);
        return new NFCViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NFCViewHolder viewHolder, int i) {
        NFCData nfcData = items.get(i);
        Shipment shipment = nfcData.getShipment();
        viewHolder.salida.setText("Embarque: "+shipment.getDateStart());
        viewHolder.entrada.setText("Llegada: "+shipment.getDateEnd());
        viewHolder.producto.setText("Envío: "+nfcData.getShipCode());
        viewHolder.ref.setText("Sensor: "+nfcData.getCallsign());
        viewHolder.pesoc.setText("Peso de captura (kg): "+shipment.getCaptureWeight());
        viewHolder.fechac.setText("Fecha de captura: "+shipment.getCaptureDate());
        viewHolder.pesom.setText("Peso de empaque (kg): "+shipment.getBoxingWeight());
        viewHolder.fecham.setText("Fecha de empaque: "+shipment.getBoxingDate());
        viewHolder.itemView.setBackgroundColor(mSelectedItemsIds.get(i) ? 0x9934B5E4
                : Color.TRANSPARENT);
    }


    public void setOnItemClickListener(AdapterView.OnClickListener listener) {
        this.listener = listener;
    }

    public void setOnLongClickListener(AdapterView.OnLongClickListener longClickListener){
        this.onLongClickListener = longClickListener;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        listener.onClick(view);
    }

    public List<NFCData> getItems() {
        return items;
    }


    public void toggleSelection(int position){
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);

        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<Integer>(mSelectedItemsIds.size());
        for (int i = 0; i < mSelectedItemsIds.size(); i++) {
            items.add(mSelectedItemsIds.keyAt(i));
        }
        return items;
    }

    @Override
    public long getItemId(int position) {
        return getItems().get(position).getId();
    }

    private int checkExists(NFCData data){
        for (NFCData d: items) {
            if (d.getId().equals(data.getId()))
                return items.indexOf(d);
        }
        return -1;
    }
}
