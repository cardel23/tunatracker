package com.pwk.track.nfc.api;

import android.util.Base64;
import android.util.Log;

import com.pwk.track.nfc.data.AOSProduct;
import com.pwk.track.nfc.data.CRMKey;
import com.pwk.track.nfc.data.CRMNote;
import com.pwk.track.nfc.data.CRMToken;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pwk.track.nfc.data.PWPXE;
import com.pwk.track.nfc.data.Shipment;

import java.io.IOException;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pwk04 on 05-16-19.
 */

public class ServiceClient {
    public static String OB_SERVER;
    private static String OB_PORT;
    public static String OB_URL = "http://"+OB_SERVER+"/openbravo/";
    //    	public static final String OB_SERVER = "http://52.16.177.26/openbravo";
//    	public static final String OB_SERVER = "http://192.168.10.104:8080/openbravo";
    public static final String REST_BASE_URL = OB_SERVER + "org.openbravo.service.json.jsonrest/";
    public static final String SHOWIMAGE_URL = OB_SERVER + "utility/ShowImage";
    private static RESTService obService = null;

    private ServiceClient() {
    }

    public static RESTService initializeService() {
        //Authenticator authenticator = new OBAuthenticator(username, password);
//        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
		/*HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);
		httpClient.interceptors().add(logging);*/

        Gson gson = new GsonBuilder().setFieldNamingPolicy(
                FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .excludeFieldsWithoutExposeAnnotation()
                .registerTypeAdapter(List.class, new DataDeserializer())
//				.registerTypeAdapter(List.class, GoodReceiptLineSerializer.instance()) //No se necesita serializar los elementos
                .create();
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
//					request.header("Authorization: " + string);
//					request.header("Accept: application/json");

                Request newRequest = request.newBuilder()
//                            .header("Authorization", string)
                        .header("Accept", "application/json")
                        .build();


                Response originalResponse = chain.proceed(newRequest);

                ResponseBody body = originalResponse.body();
                String bodyString = body.string();
                String requestString = originalResponse.request().url().toString();
                Log.i("ServiceClient", "URL: " + requestString);
                MediaType contentType = body.contentType();
                Response newResponse = originalResponse.newBuilder().body(ResponseBody.create(contentType, bodyString)).build();
                Log.i("ServiceClient", "Response: " + bodyString);
                return newResponse;
            }
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://opensky-network.org/api/")
                .client(httpClientBuilder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));

        Retrofit adapter = builder.build();
        obService = adapter.create(RESTService.class);
        return obService;
    }
    public static SuiteCRMService tokenService() {
//        OkHttpClient httpClient = new OkHttpClient();

//        httpClient.setReadTimeout(40, TimeUnit.SECONDS);
//        httpClient.setConnectTimeout(40, TimeUnit.SECONDS);
        Gson gson = new GsonBuilder().setFieldNamingPolicy(
                FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .excludeFieldsWithoutExposeAnnotation()
                .registerTypeAdapter(CRMToken.class, new CRMTokenDeserializer())
                .registerTypeAdapter(CRMKey.class, new CRMKeySerializer())
//				.registerTypeAdapter(List.class, GoodReceiptLineSerializer.instance()) //No se necesita serializar los elementos
                .create();
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
//					request.header("Authorization: " + string);
//					request.header("Accept: application/json");

                Request newRequest = request.newBuilder()
//                            .header("Authorization", string)

                        .header("Accept", "application/vnd.api+json")
                        .header("Content-Type", "application/vnd.api+json")
                        .build();


                Response originalResponse = chain.proceed(newRequest);

                ResponseBody body = originalResponse.body();
                String bodyString = body.string();
                String requestString = originalResponse.request().url().toString();
                Log.i("ServiceClient", "URL: " + requestString);
                MediaType contentType = body.contentType();
                Response newResponse = originalResponse.newBuilder().body(ResponseBody.create(contentType, bodyString)).build();
                Log.i("ServiceClient", "Response: " + bodyString);
                return newResponse;
            }
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://server3.peoplewalking.com/demo/balfego/tuna-tracker/suitecrm/")
//                .baseUrl("http://data.peoplewalking.com/demo/balfego/tuna-tracker/suitecrm/")
                .client(httpClientBuilder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));

        Retrofit adapter = builder.build();
        SuiteCRMService service = adapter.create(SuiteCRMService.class);
        return service;
    }

    public static SuiteCRMService crmService(final CRMToken token) {
//        OkHttpClient httpClient = new OkHttpClient();

//        httpClient.setReadTimeout(40, TimeUnit.SECONDS);
//        httpClient.setConnectTimeout(40, TimeUnit.SECONDS);
        Gson gson = new GsonBuilder().setFieldNamingPolicy(
                FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .excludeFieldsWithoutExposeAnnotation()
                .registerTypeAdapter(CRMNote.class, new CRMNoteSerializer())
                .registerTypeAdapter(List.class, new SuiteListDeserializer<AOSProduct>())
                .registerTypeAdapter(List.class, new SensorDeserializer())
                .registerTypeAdapter(List.class, new SuiteListDeserializer<PWPXE>())
                .registerTypeAdapter(List.class, new SuiteListDeserializer<Shipment>())
//				.registerTypeAdapter(List.class, GoodReceiptLineSerializer.instance()) //No se necesita serializar los elementos
                .create();
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Request newRequest = request.newBuilder()
                        .header("Accept", "application/vnd.api+json")
                        .header("Content-Type", "application/vnd.api+json")
                        .header("Authorization","Bearer "+token.getAccessToken())
                        .build();
                Response originalResponse = chain.proceed(newRequest);
                ResponseBody body = originalResponse.body();
                String bodyString = body.string();
                String requestString = originalResponse.request().url().toString();
                Log.i("ServiceClient", "URL: " + requestString);
                MediaType contentType = body.contentType();
                Response newResponse = originalResponse.newBuilder().body(ResponseBody.create(contentType, bodyString)).build();
                Log.i("ServiceClient", "Response: " + bodyString);
                return newResponse;
            }
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://server3.peoplewalking.com/demo/balfego/tuna-tracker/suitecrm/")
//                .baseUrl("http://data.peoplewalking.com/demo/balfego/tuna-tracker/suitecrm/")
                .client(httpClientBuilder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));

        Retrofit adapter = builder.build();
        SuiteCRMService service = adapter.create(SuiteCRMService.class);
        return service;
    }
}