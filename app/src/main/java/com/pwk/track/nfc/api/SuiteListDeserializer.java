package com.pwk.track.nfc.api;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.pwk.track.nfc.data.Constants;
import com.pwk.track.nfc.data.SuiteEntity;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SuiteListDeserializer<T extends SuiteEntity> implements JsonDeserializer<List<T>> {
    public static final String TAG = "SuiteListDeserializer";
    public List<T> deserialize(JsonElement json, Type type,
                               JsonDeserializationContext context) throws JsonParseException {
        try {
            JsonObject jsonResponse = new JsonParser().parse(json.toString()).getAsJsonObject();
            if(jsonResponse.has("data")){
                JsonArray data;
                try{
                    data = jsonResponse.getAsJsonArray("data").getAsJsonArray();
                } catch (Exception e){
                    data = new JsonArray();
                    data.add(jsonResponse.getAsJsonObject("data"));
                }
                List<T> elementsArray;
                try {
                    elementsArray = new ArrayList<>();
                    try {
                        for (JsonElement jsonElement : data) {
                            String entityName = jsonElement.getAsJsonObject().get("type").getAsString();
                            String className = Constants.getClassNames(entityName);
                            if(jsonElement.getAsJsonObject().has("attributes")) {
                                JsonElement attributes = jsonElement.getAsJsonObject().getAsJsonObject("attributes");
                                try {
                                    T element = context.deserialize(attributes, Class.forName(className));
                                    element.setEntityId(jsonElement.getAsJsonObject().get("id").getAsString());
                                    element.setEntityName(entityName);
                                    elementsArray.add(element);
                                } catch (JsonSyntaxException e2) {
                                    e2.printStackTrace();
                                }
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        throw new JsonParseException(e1.getMessage());
                    }
                } catch (Exception e){
                    return null;
                }
                return elementsArray;
            }
        } catch (JsonParseException e) {
            Log.e(TAG, e.getMessage(), e);
            throw e;
        }
        return null;
    }
}
