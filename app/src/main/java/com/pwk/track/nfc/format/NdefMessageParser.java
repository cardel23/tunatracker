package com.pwk.track.nfc.format;

import android.nfc.NdefMessage;
import android.nfc.NdefRecord;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pwk04 on 06-04-19.
 */

public class NdefMessageParser {

    private NdefMessageParser() {
    }

    public static List<ParsedNdefRecord> parse(NdefMessage message) {
        return getRecords(message.getRecords());
    }

    public static List<ParsedNdefRecord> getRecords(NdefRecord[] records) {
        List<ParsedNdefRecord> elements = new ArrayList<>();

        for (final NdefRecord record : records) {
            elements.add(new ParsedNdefRecord() {
                public String str() {
                    String str = null;
                    try {
                        str = new String(record.getPayload(),"ISO-8859-1");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        str = new String(record.getPayload());
                    }
//                    Log.i("PAYLOAD RECIBIDO: ",str);
                    return str;
                }
            });
        }

        return elements;
    }

    public static List<ExtendedNdefRecord> parseExt(NdefMessage message) {
        return getExtRecords(message.getRecords());
    }

    public static List<ExtendedNdefRecord> getExtRecords(NdefRecord[] records) {
        List<ExtendedNdefRecord> elements = new ArrayList<>();

        for (final NdefRecord record : records) {
            elements.add(new ExtendedNdefRecord() {
                public String str() {
                    String str = null;
                    try {
                        str = new String(record.getPayload(),"ISO-8859-1");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        str = new String(record.getPayload());
                    }
//                    Log.i("PAYLOAD RECIBIDO: ",str);
                    return str;
                }

                public String id() {
                    String str = null;
                    try {
                        str = new String(record.getId(),"ISO-8859-1");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        str = new String(record.getId());
                    }
//                    Log.i("PAYLOAD RECIBIDO: ",str);
                    return str;
                }

                public String type() {
                    String str = null;
                    try {
                        str = new String(record.getType(),"ISO-8859-1");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        str = new String(record.getType());
                    }
//                    Log.i("PAYLOAD RECIBIDO: ",str);
                    return str;
                }
            });
        }

        return elements;
    }
}
