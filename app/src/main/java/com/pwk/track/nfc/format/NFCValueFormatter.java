package com.pwk.track.nfc.format;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.pwk.track.nfc.App;
import com.pwk.track.nfc.data.NFCPathRecord;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by pwk04 on 05-19-19.
 */

public class NFCValueFormatter extends ValueFormatter {

    private App app;

    public NFCValueFormatter(App app){
        super();
        this.app = app;
    }

    private SimpleDateFormat mFormat = new SimpleDateFormat("dd/MM HH:mm", Locale.ENGLISH);

    public String getFormattedValue(float value) {
        SimpleDateFormat sdf = getFormat();
        NFCPathRecord record = app.getCurrent().getRecords().get((int) value);
        return sdf.format(record.getDate());
    }

    public String getAxisLabel(float value, AxisBase axis) {
        return getFormattedValue(value);
    }

    public App getApp(){
        return app;
    }

    public SimpleDateFormat getFormat(){
        return mFormat;
    }

}
