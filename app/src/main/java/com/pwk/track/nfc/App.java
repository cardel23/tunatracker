package com.pwk.track.nfc;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.widget.Toast;

import com.orm.SugarApp;
import com.pwk.track.nfc.data.NFCData;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * Created by pwk04 on 05-16-19.
 * @author Carlos Delgadillo
 */

public class App extends SugarApp {

    private static App mContext;
    private NFCData current;
    private static final String TAG = "NFCApp";
    public static final String LOG_NAME = new SimpleDateFormat("yyyyMMdd").format(new Date());
    private static final String KEY_APP_CRASHED = "UnhandledException";
    private SharedPreferences sharedPreferences;
    public final static int MY_PERMISSIONS_REQUEST_INTERNET = 1;
    private Bitmap bitmap;
    public static final int[] REQUEST_CODES = new int[]{
            MY_PERMISSIONS_REQUEST_INTERNET
    };
    public static final String[] PERMITS = new String[]{
            Manifest.permission.INTERNET,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_WIFI_STATE
    };
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    //    @Override
//    public void onCreate() {
//        super.onCreate();
//        StrictMode.ThreadPolicy policy = new
//                StrictMode.ThreadPolicy.Builder()
//                .permitAll().build();
//        StrictMode.setThreadPolicy(policy);
//        ContextWrapper cw = new ContextWrapper(getApplicationContext());
////        imageDir = cw.getDir("images", Context.MODE_PRIVATE);
//        final Thread.UncaughtExceptionHandler defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
//        Thread.setDefaultUncaughtExceptionHandler( new Thread.UncaughtExceptionHandler() {
//            public void uncaughtException(Thread thread, Throwable exception) {
//                // Save the fact we crashed out.
//                getSharedPreferences( TAG , Context.MODE_PRIVATE ).edit()
//                        .putBoolean( KEY_APP_CRASHED, true ).apply();
//                // Chain default exception handler.
//                if ( defaultHandler != null ) {
//                    defaultHandler.uncaughtException( thread, exception );
//                }
//            }
//        } );
//
//        boolean bRestartAfterCrash = getSharedPreferences( TAG , Context.MODE_PRIVATE )
//                .getBoolean( KEY_APP_CRASHED, false );
//        if ( bRestartAfterCrash ) {
//            // Clear crash flag.
//            getSharedPreferences( TAG , Context.MODE_PRIVATE ).edit()
//                    .putBoolean( KEY_APP_CRASHED, false ).apply();
//            // Re-launch from root activity with cleared stack.
//            Intent intent = new Intent( this, NFCActivity.class );
////            intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );
//            Toast.makeText(this, "Se ha producido un error inesperado", Toast.LENGTH_SHORT).show();
//            startActivity( intent );
//            Toast.makeText(this, "Se ha producido un error inesperado", Toast.LENGTH_SHORT).show();
//        }
//
//    }

    public NFCData getCurrent() {
        return current;
    }

    public void setCurrent(NFCData current) {
        this.current = current;
    }

    public SharedPreferences getPrefs(){
        if(sharedPreferences == null)
            sharedPreferences = getSharedPreferences(TAG, Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    /**
     * Edita un parámetro de configuración (SharedPreference)
     * @param preferenceName nombre de la preferencia
     * @param val valor asignado
     * @author Carlos Delgadillo
     * @fecha 06/05/2015
     */
    public void editSharedPreference(String preferenceName, String val){
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString(preferenceName, val);
        editor.commit();
    }

    /**
     * Edita un parámetro de configuración (SharedPreference)
     * @param preferenceName
     * @param val valor asignado
     * @author Carlos Delgadillo
     * @fecha 06/05/2015
     */
    public void editSharedPreference(String preferenceName, int val){
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putInt(preferenceName, val);
        editor.commit();
    }

    public static int getRandom(int min, int max){
        int i = new Random().nextInt(max - min + 1) + min;
        return i;
    }
    public static ArrayList<String> getServices(){
        ArrayList<String> a = new ArrayList<>();
        a.add("3c4ad0");
        a.add("3c4b26");
        a.add("3c70c3");
        a.add("3c6571");
        a.add("3c6487");
        a.add("3c4b23");
        a.add("3c64ed");
        a.add("3c658b");
        a.add("3c5465");
        a.add("3c648e");
        return a;
    }
    public static int getMyPermissionsRequestInternet() {
        return MY_PERMISSIONS_REQUEST_INTERNET;
    }

    @SuppressLint("SimpleDateFormat")
    public void appendLog(String text)
    {
        String fileName = Environment.getExternalStorageDirectory().getAbsolutePath()+"/nfc_" + getPrefs().getString(LOG_NAME,LOG_NAME) +".txt";
        File logFile = new File(fileName);
        if (!logFile.exists())
        {
            try
            {
                createLogFile(logFile);
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            StringBuilder sb = new StringBuilder();
            sb.append(getFormattedDate());
            sb.append(" - ");
            sb.append(text);
            buf.append(sb.toString());
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setLogName(String string){
        editSharedPreference(App.LOG_NAME, string);
    }

    public static App getContext() {
        return mContext;
    }

    private void createLogFile(File logFile) throws IOException {
        logFile.createNewFile();
        BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
        StringBuilder sb = new StringBuilder();
        sb.append("Log de aplicacion NFC\n");
        sb.append("Iniciado: "+getFormattedDate());
        sb.append("\n-----------------------------------------\n\n\n");
        buf.append(sb.toString());
        buf.newLine();
        buf.close();
    }

    @SuppressLint("SimpleDateFormat")
    private String getFormattedDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date date = new Date(System.currentTimeMillis());
        return sdf.format(date);
    }

    public boolean isWriteModeEnabled(){
        int mode = getPrefs().getInt("nfc_mode",0);
        return mode != 0;
    }

    public void setMode(boolean mode){
        int val = 0;
        if(mode)
            val = 1;
        editSharedPreference("nfc_mode", val);
    }

    public void appendException(Throwable t){
        appendLog(t.getMessage());
        for(StackTraceElement i : t.getStackTrace())
            appendLog(" - " + i.getClassName() +":"+
                    String.valueOf(i.getLineNumber()));
    }

    public String saveImageToInternalSorage(Bitmap bitmapImage, String dir, String imageName){
        File imageDir=new File(Environment.getExternalStorageDirectory().getAbsolutePath(), dir);
        if(!imageDir.exists()){
            imageDir.mkdirs();
        }

        File imagePath=new File(imageDir, imageName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imagePath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return relative image path
        return dir.concat("/").concat(imageName);
    }

    public Bitmap loadImageFromInternalStorage(String relativeImagePath){
        try {
            if(relativeImagePath != null && !"".equals(relativeImagePath)){
                File f=new File(Environment.getExternalStorageDirectory().getAbsolutePath(), relativeImagePath);
                return BitmapFactory.decodeStream(new FileInputStream(f));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return BitmapFactory.decodeResource(getResources(), R.drawable.balf1);
    }

}
