package com.pwk.track.nfc.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pwk.track.nfc.R;
import com.pwk.track.nfc.data.NFCPathRecord;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by pwk04 on 05-16-19.
 * @author Carlos Delgadillo
 */

public class NFCRecordAdapter extends ArrayAdapter<NFCPathRecord> {

    private SparseBooleanArray mSelectedItemsIds;
    List<NFCPathRecord> nfcDataList;
    Context context;
    private TextView title, subtitle, type, text4, text5, text6;
    public NFCRecordAdapter(Context context, List<NFCPathRecord> nfcDataList) {
        super(context, 0, nfcDataList);
        mSelectedItemsIds = new SparseBooleanArray();
        this.nfcDataList = nfcDataList;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = this.getContext().getApplicationContext();
        NFCPathRecord nfcData = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_record, parent, false);
        }
        title = (TextView) convertView.findViewById(R.id.textItem1);
        subtitle = (TextView) convertView.findViewById(R.id.textItem2);
        type = (TextView) convertView.findViewById(R.id.textItem3);
        text4 = (TextView) convertView.findViewById(R.id.textItem4);
        text5 = (TextView) convertView.findViewById(R.id.textItem5);
        text6= (TextView) convertView.findViewById(R.id.textItem6);
        title.setText("Hora: "+nfcData.getDate());
//		title.setTextColor(Color.BLACK);
        title.setTextSize(20);
//		subtitle.setTextSize(16);

        subtitle.setText("Latitud :"+nfcData.getLatitude());
        SimpleDateFormat sdf = new SimpleDateFormat();
        Date date = new Date();
        type.setText("Longitud: "+nfcData.getLongitude());
        text4.setText("Altitud (m): "+nfcData.getAltitude());
        text5.setText("Temperatura (ºC): "+nfcData.getTemp());
        text6.setText("Estado: "+ (nfcData.isOnGround() ? "En tierra" : "Volando"));
        return convertView;
    }

    public void toggleSelection(int position){
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);

        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

}