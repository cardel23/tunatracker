package com.pwk.track.nfc.api;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pwk.track.nfc.data.AOSProduct;
import com.pwk.track.nfc.data.CRMKey;
import com.pwk.track.nfc.data.CRMNote;
import com.pwk.track.nfc.data.CRMToken;
import com.pwk.track.nfc.data.NFCData;
import com.pwk.track.nfc.data.PWPXE;
import com.pwk.track.nfc.data.Shipment;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by pwk04 on 05-28-19.
 */

public interface SuiteCRMService {

    @POST("Api/access_token")
    public Call<CRMToken> getAccessToken(@Body CRMKey key);

    @Headers("Content-Type: application/json")
    @POST("Api/V8/module")
    public Call<CRMNote> postNote(@Body CRMNote note);

    @GET("Api/V8/module/AOS_Products")
    public Call<List<AOSProduct>> getAOSProducts(@QueryMap Map<String, String> query);

    @GET("Api/V8/module/AOS_Products/{id}")
    public Call<List<AOSProduct>> getAOSProducts(@Path("id") String id);

    @GET("Api/V8/module/PW_ProductosXEnvio")
    public Call<List<PWPXE>> getProdMeeting(@QueryMap Map<String, String> query);

    @GET("Api/V8/module/Meetings")
    public Call<List<Shipment>> getShipments(@QueryMap Map<String, String> query);

    @GET("Api/V8/custom/meetings")
    public Call<JsonElement> getLastShipment(@QueryMap Map<String, String> query);

}
