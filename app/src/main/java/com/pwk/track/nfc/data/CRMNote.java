package com.pwk.track.nfc.data;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pwk04 on 05-28-19.
 */

public class CRMNote {
    @Expose
    @SerializedName("type")
    private String type;

    @Expose
    @SerializedName("attributes")
    private JsonObject attributes;

    private String description;

    public CRMNote(){
        type = "Note";
    }

    public String getType() {
        return type;
    }

    public JsonObject getAttributes() {
        return attributes;
    }

    public void setAttributes(JsonObject attributes) {
        this.attributes = attributes;
    }
}
