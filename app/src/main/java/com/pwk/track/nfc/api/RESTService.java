package com.pwk.track.nfc.api;

import com.pwk.track.nfc.data.NFCData;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by pwk04 on 05-16-19.
 */

public interface RESTService {

    @GET("tracks/all")
    public Call<List<NFCData>> getNFCData(@Query("") String query);
    @GET("tracks/all")
    public Call<List<NFCData>> getNFCData(@QueryMap Map<String, String> query);

    @GET("tracks/all")
    public Call<NFCData> getNFCDevice(@QueryMap Map<String, String> query);
}
