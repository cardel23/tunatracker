package com.pwk.track.nfc.api;

/**
 * Created by pwk04 on 05-16-19.
 */

import java.util.List;


public interface OnPostExecuteListener<T> {
    public void onPostExecute(T response, List<String> errorMessages);
}

