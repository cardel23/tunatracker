package com.pwk.track.nfc.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shipment extends SuiteEntity {

    @Expose
    @SerializedName("date_start")
    private String dateStart;

    @Expose
    @SerializedName("date_end")
    private String dateEnd;

    @Expose
    @SerializedName("status")
    private String status;

    @Expose
    @SerializedName("codigo_envio_c")
    private String shipmentCode;

    @Expose
    @SerializedName("fecha_captura_c")
    private String captureDate;

    @Expose
    @SerializedName("peso_captura_c")
    private double captureWeight;

    @Expose
    @SerializedName("fecha_empaque_c")
    private String boxingDate;

    @Expose
    @SerializedName("peso_empaque_c")
    private double boxingWeight;

    public String getDateStart() {
        return getConvertedDate(dateStart);
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return getConvertedDate(dateEnd);
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShipmentCode() {
        return shipmentCode;
    }

    public void setShipmentCode(String shipmentCode) {
        this.shipmentCode = shipmentCode;
    }

    public String getCaptureDate() {
        return getConvertedDate(captureDate);
    }

    public void setCaptureDate(String captureDate) {
        this.captureDate = captureDate;
    }

    public double getCaptureWeight() {
        return captureWeight;
    }

    public void setCaptureWeight(double captureWeight) {
        this.captureWeight = captureWeight;
    }

    public String getBoxingDate() {
        return getConvertedDate(boxingDate);
    }

    public void setBoxingDate(String boxingDate) {
        this.boxingDate = boxingDate;
    }

    public double getBoxingWeight() {
        return boxingWeight;
    }

    public void setBoxingWeight(double boxingWeight) {
        this.boxingWeight = boxingWeight;
    }
}