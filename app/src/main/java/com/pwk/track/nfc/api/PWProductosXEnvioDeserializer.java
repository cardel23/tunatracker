package com.pwk.track.nfc.api;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PWProductosXEnvioDeserializer implements JsonDeserializer<List<String>> {

    public static final String TAG = PWProductosXEnvioDeserializer.class.getSimpleName();

    @Override
    public List<String> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        try {
            JsonElement jsonResponse = new JsonParser().parse(json.toString()).getAsJsonObject();
            JsonArray data = jsonResponse.getAsJsonObject().getAsJsonArray("data").getAsJsonArray();
            List<String> elementsArray;
            try {
                elementsArray = new ArrayList<>();
                try {
                    for (JsonElement jsonElement : data) {
                        try {
                            JsonElement attributes = jsonElement.getAsJsonObject().get("attributes").getAsJsonObject();
                            elementsArray.add(attributes.getAsJsonObject().get("producto_id_c").getAsString());
                        } catch (JsonSyntaxException e2) {
                            e2.printStackTrace();
                        }
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                    throw new JsonParseException(e1.getMessage());
                }
                return elementsArray;
            } catch (Exception e) {

            }
        } catch (JsonParseException e) {
            Log.e(TAG, e.getMessage(), e);
            throw e;
        }
        return null;
    }
}
