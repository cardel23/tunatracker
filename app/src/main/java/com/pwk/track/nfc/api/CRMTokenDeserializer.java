package com.pwk.track.nfc.api;

import com.pwk.track.nfc.data.CRMToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by pwk04 on 05-28-19.
 */

public class CRMTokenDeserializer implements JsonDeserializer<CRMToken> {
    @Override
    public CRMToken deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        CRMToken token = new CRMToken();
        token.setTokenType(json.getAsJsonObject().get("token_type").getAsString());
        token.setExpireTime(json.getAsJsonObject().get("expires_in").getAsInt());
        token.setAccessToken(json.getAsJsonObject().get("access_token").getAsString());
        return token;
    }
}
