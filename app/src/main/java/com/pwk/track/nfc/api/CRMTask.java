package com.pwk.track.nfc.api;

import android.content.Context;

import com.pwk.track.nfc.data.CRMKey;
import com.pwk.track.nfc.data.CRMNote;
import com.pwk.track.nfc.data.CRMToken;

import java.util.ArrayList;
import java.util.List;

import retrofit2.HttpException;
import retrofit2.Response;

/**
 * Created by pwk04 on 05-28-19.
 */

public class CRMTask extends TaskWithCallback<Void,Void,List<String>> {
    private CRMKey key;
    private CRMToken token;
    private CRMNote note;
//    private App app;

    public CRMTask(Context context, CRMKey key, CRMNote note) {
        super(context,"Enviando...");
        this.key = key;
        this.note = note;
    }

    @Override
    protected List<String> doInBackground(Void... voids) {
        errorMessages = new ArrayList<>();
        List<String> messages = new ArrayList<>();
        try {
            CRMKey key = new CRMKey();
            SuiteCRMService service = ServiceClient.tokenService();
            token = service.getAccessToken(key).execute().body();
            service = ServiceClient.crmService(token);
            Response<CRMNote> response = service.postNote(note).execute();
            if (response.code() == 201) {
                messages.add("El registro ha sido guardado");
            } else {
                throw new HttpException(response);
            }
        } catch (HttpException e){
            errorMessages.add(String.valueOf(e.code()) + " - " + e.message());
        } catch (Exception e){
            errorMessages.add(e.getMessage());
        }
        return messages;
    }

}
