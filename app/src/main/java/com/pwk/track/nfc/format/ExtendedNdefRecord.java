package com.pwk.track.nfc.format;

/**
 * Created by pwk04 on 07-15-19.
 */

public interface ExtendedNdefRecord extends ParsedNdefRecord {

    String id();
    String type();
}
