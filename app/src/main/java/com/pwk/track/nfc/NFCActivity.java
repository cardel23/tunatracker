package com.pwk.track.nfc;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;
import android.widget.Toast;

import com.pwk.track.nfc.adapter.NFCDataAdapter;
import com.pwk.track.nfc.api.NFCTask;
import com.pwk.track.nfc.api.OnPostExecuteListener;
import com.pwk.track.nfc.data.AOSProduct;
import com.pwk.track.nfc.data.NFCData;
import com.pwk.track.nfc.data.NFCPathRecord;
import com.pwk.track.nfc.data.PWPXE;
import com.pwk.track.nfc.data.Shipment;
import com.pwk.track.nfc.exception.ConvertException;
import com.pwk.track.nfc.format.Converter;
import com.pwk.track.nfc.format.ExtendedNdefRecord;
import com.pwk.track.nfc.format.NdefMessageParser;
import com.pwk.track.nfc.format.ParsedNdefRecord;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author Carlos Delgadillo
 */
public class NFCActivity extends DemoBase implements ActionMode.Callback{

    private List<NFCData> nfcDataList;
    private App app;
    private RecyclerView recycler;
    private NFCDataAdapter dataAdapter;
    private RecyclerView.LayoutManager lManager;
    private ActionMode actionMode;
    FloatingActionButton fab;
    private boolean isActionMode = false;
    private NfcAdapter nfcAdapter;
    private PendingIntent pendingIntent;
    EditText dStart, dEnd;
    private boolean isNFCActive = false;
    private StringBuilder rawPayloadBuilder;
    private Menu mOptionsMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc);
        app = (App) getApplicationContext();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.inicio);
        if(nfcDataList == null) {
            nfcDataList = new ArrayList<>();
            nfcDataList.addAll(NFCData.findWithQuery(NFCData.class,"SELECT * FROM NFC_DATA"));
            for(NFCData d : nfcDataList){
                addProductList(d);
            }
        }
        recycler = (RecyclerView) findViewById(R.id.recycler2);
        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);
        dataAdapter = new NFCDataAdapter(nfcDataList,app);
        dataAdapter.setHasStableIds(true);
        recycler.setAdapter(dataAdapter);
        recycler.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        dataAdapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
                int position = viewHolder.getAdapterPosition();
                if(isActionMode){
                    myToggleSelection(position);
                } else  {
                    NFCData item = nfcDataList.get(position);
                    app.setCurrent(item);
                    Intent intent = new Intent(NFCActivity.this, InfoActivity.class);
                    startActivity(intent);
                }

            }
        });
        dataAdapter.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (actionMode != null) {
                    return false;
                }
                try {
                    actionMode = startSupportActionMode(NFCActivity.this);
                    int idx = recycler.getChildPosition(view);
                    myToggleSelection(idx);
                    return true;
                } catch (Exception ex){
                    ex.printStackTrace();
                    return false;
                }
            }
        });
        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(6);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkPermissions(Manifest.permission.INTERNET,1)) {
                    try{
                        rawPayloadBuilder = new StringBuilder(Converter.fakeRecords().toUpperCase());
                        MenuItem item = mOptionsMenu.getItem(0);
                        switchMode(item, true);
                        isNFCActive = true;
                        Toast.makeText(NFCActivity.this,"Acerque la tarjeta para grabar la trama",Toast.LENGTH_SHORT).show();
                    } catch (Exception e){
                        app.appendLog(e.getMessage());
                    }
                }
            }
        });
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if(nfcAdapter == null){
            Toast.makeText(this,"No hay NFC",Toast.LENGTH_SHORT).show();
            return;
        }
        pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, this.getClass())
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);

    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.item_remove, menu);
        View fab = findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        isActionMode = true;
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:
                List<Integer> selectedItemPositions =
                        dataAdapter.getSelectedItems();
                for (int i = selectedItemPositions.size() - 1;
                     i >= 0;
                     i--) {
                    NFCData data = dataAdapter.getItems().get(i);
                    deleteData(data);
                    dataAdapter.getItems().remove(i);
                }
                dataAdapter.notifyDataSetChanged();
                actionMode.finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        this.actionMode = null;
        dataAdapter.removeSelection();
        View fab = findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);
        isActionMode = false;
    }

    private void myToggleSelection(int idx) {
        dataAdapter.toggleSelection(idx);
        if(dataAdapter.getSelectedCount() < 1){
            actionMode.finish();
        } else {
            String title = dataAdapter.getSelectedCount()>1 ? getString(R.string.multi_selected,
                    String.valueOf(dataAdapter.getSelectedCount())) : getString(R.string.one_selected,
                    String.valueOf(dataAdapter.getSelectedCount()));
            actionMode.setTitle(title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        mOptionsMenu = menu;
        System.out.print("Write mode "+App.getContext().isWriteModeEnabled());
        switchMode(menu.getItem(0), app.isWriteModeEnabled());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        System.out.print("Write mode "+App.getContext().isWriteModeEnabled());
        switch (item.getItemId()) {
            case R.id.detail:
                switchMode(item, !app.isWriteModeEnabled());
                break;
        }
        return true;
    }

    private void switchMode(MenuItem item, boolean isWriteModeEnabled){
        app.setMode(isWriteModeEnabled);
        String message;
        if(!isWriteModeEnabled)
            message = "Activar modo escritura";
        else
            message = "Activar modo lectura";
        item.collapseActionView();
        item.setTitle(message);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case App.MY_PERMISSIONS_REQUEST_INTERNET:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    NFCTask task = new NFCTask(NFCActivity.this);
                    task.setOnPostExecuteListener(new OnPostExecuteListener<NFCData>() {
                        @Override
                        public void onPostExecute(NFCData response, List<String> errorMessages) {
                            dataAdapter.add(response);
                            dataAdapter.notifyDataSetChanged();
                        }
                    });
                    task.execute();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            case 3:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    readNFC();
                }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(app.getCurrent()!= null) {
            for (int i = 0; i < dataAdapter.getItemCount(); i++) {
                NFCData data = dataAdapter.getItems().get(i);
                if (data.getStartTime().equals(app.getCurrent().getStartTime()) & app.getCurrent().isSent()) {
                    dataAdapter.getItems().remove(i);
                    NFCPathRecord.deleteAll(NFCPathRecord.class,"data_id=?",String.valueOf(data.getId()));
                    data.delete();
                    dataAdapter.notifyDataSetChanged();
                    break;
                }
            }
        }
        if(nfcAdapter != null) {
            if (!nfcAdapter.isEnabled()) {
                displayWirelessSettings();
            }
            nfcAdapter.enableForegroundDispatch(this, pendingIntent,null, null);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(nfcAdapter != null) {
            if (nfcAdapter.isEnabled()) {
                nfcAdapter.disableForegroundDispatch(this);
            }
        }
    }

    private void readNFC(){

    }

    private void displayWirelessSettings(){
        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
        startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        resolveIntent(intent);
    }

    void resolveIntent(Intent intent){
        String action = intent.getAction();
        switch (action) {
            case NfcAdapter.ACTION_NDEF_DISCOVERED:
            case NfcAdapter.ACTION_TAG_DISCOVERED:
            case NfcAdapter.ACTION_TECH_DISCOVERED:
                try {
                    Parcelable[] rawMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
                    NdefMessage[] messages;
                    if (!app.isWriteModeEnabled()) {
                        if (rawMessages != null) {
                            messages = new NdefMessage[rawMessages.length];
                            for (int i = 0; i < rawMessages.length; i++) {
                                messages[i] = (NdefMessage) rawMessages[i];
                            }
                            Tag myTag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                            String tagId = new String(myTag.getId(),"ISO-8859-1");
                            List<ExtendedNdefRecord> records = NdefMessageParser.parseExt(messages[0]);
                            final NFCTask task = new NFCTask(this,records, Converter.asciiToHex(tagId));
                            execTask(task);
//                                displayMessages(myTag, messages);
                        } else {
                            app.appendLog("Error al leer mensajes");
                            Toast.makeText(this, "Error al leer mensajes: Valor nulo.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        prepareWriter(intent);
                    }
//                    displayMessages(messages);
                } catch (Exception e) {
                    e.printStackTrace();
                    app.appendException(e);
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
        }

    }

    void displayMessages(Tag tag, NdefMessage[] messages) throws UnsupportedEncodingException {
        isNFCActive = false;
        if(messages == null | messages.length == 0)
            return;
        app.appendLog("Iniciando proceso de lectura");
        StringBuilder builder = new StringBuilder();
        String tagId = new String(tag.getId(),"ISO-8859-1");
        List<ExtendedNdefRecord> records = NdefMessageParser.parseExt(messages[0]);
        final int size = records.size();
        final NFCTask task = new NFCTask(this,records, Converter.asciiToHex(tagId));
        for(int i=0; i<size; i++){
            ParsedNdefRecord parsedNdefRecord = records.get(i);
            try {
                String str = Converter.asciiToHex(parsedNdefRecord.str());
                app.appendLog("Mensaje recibido: "+parsedNdefRecord.str());
                app.appendLog("ParsedNdefRecord: "+str);
//                String str = "NFC Detectado, ¿confirmar lectura?";
                builder.append(str).append("\n");
            } catch (Exception e){
                builder.append(e.getMessage());
            }
        }
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("NFC Detectado, ¿confirmar lectura?");
        alertDialogBuilder.setMessage(builder.toString());
        alertDialogBuilder.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                isNFCActive = false;
                dialog.dismiss();
                execTask(task);
            }
        });
        alertDialogBuilder.create().show();
    }

    @SuppressLint("SimpleDateFormat")
    void displayDateDialog(final Intent intent) throws IOException{
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = getLayoutInflater();
        View dialogView = layoutInflater.inflate(R.layout.dialog_temp, null);
        dStart = (EditText) dialogView.findViewById(R.id.et_mostrar_fecha_picker);
        final ImageButton bStart = dialogView.findViewById(R.id.ib_obtener_fecha);
        bStart.setOnClickListener(clickListener);
        builder.setView(dialogView)
        .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if((!dStart.getText().toString().equals(""))) {
                    try {
                        app.appendLog("Iniciando configuracion de fecha para escritura");
                        rawPayloadBuilder = new StringBuilder();
                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                        long time = format.parse(dStart.getText().toString()).getTime() / 1000;
                        rawPayloadBuilder.append(Long.toHexString(time).toUpperCase()).append("FE");
                        rawPayloadBuilder.append("0F").append("FE");
                        app.appendLog("Fecha a escribir "+rawPayloadBuilder.toString());
                        isNFCActive = true;
                        Toast.makeText(NFCActivity.this, "Acerque el dispositivo para grabar fecha.", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    } catch (Exception e) {
                        app.appendException(e);
                        Toast.makeText(NFCActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        isNFCActive = false;
                        dialog.dismiss();
                    }
                } else {
                    app.appendLog("Fechas no validas");
                    Toast.makeText(NFCActivity.this, "Fechas no validas", Toast.LENGTH_SHORT).show();
                }
            }
        }).setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                isNFCActive = false;
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            final Calendar c = Calendar.getInstance();
            final int month = c.get(Calendar.MONTH);
            final int day = c.get(Calendar.DAY_OF_MONTH);
            final int year = c.get(Calendar.YEAR);
            DatePickerDialog dialog = new DatePickerDialog(NFCActivity.this,new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                    final int mesActual = month + 1;
                    final StringBuilder sb = new StringBuilder();
                    Calendar currentDate = Calendar.getInstance();
                    String diaFormateado = (dayOfMonth < 10)? "0" + String.valueOf(dayOfMonth):String.valueOf(dayOfMonth);
                    sb.append(diaFormateado).append("/");
                    String mesFormateado = (mesActual < 10)? "0" + String.valueOf(mesActual):String.valueOf(mesActual);
                    sb.append(mesFormateado).append("/").append(year);
                    new TimePickerDialog(NFCActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view1, int hourOfDay, int minute) {
                            String minFormateado = (minute < 10)? "0" + String.valueOf(minute):String.valueOf(minute);
                            sb.append(" ").append(hourOfDay).append(":").append(minFormateado);
                            switch (view.getId()){
                                case R.id.ib_obtener_fecha:
                                    dStart.setText(sb.toString());
                                    break;
                            }
                        }
                    }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), true).show();
                }
            },year, month, day);
            dialog.show();
        }
    };

    public void prepareWriter(Intent intent) throws IOException, FormatException, ConvertException{
        if(!isNFCActive)
            displayDateDialog(intent);
        else {
            byte[] empty = new byte[0];
            byte[] id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
            Tag tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String str = rawPayloadBuilder.toString();
            byte[] payload = Converter.toAsciiBytes(str);
            NdefRecord ndefRecord = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, empty, payload);
            NdefMessage msg = new NdefMessage(new NdefRecord[]{ndefRecord});
            app.appendLog("Payload: " + str);
            app.appendLog("Payload codificado: " + new String(payload));
            Ndef ndefTag = Ndef.get(tag);
            if (ndefTag == null) {
                // Let's try to format the Tag in NDEF
                NdefFormatable nForm = NdefFormatable.get(tag);
                if (nForm != null) {
                    nForm.connect();
                    nForm.format(msg);
                    nForm.close();
                }
            } else {
                ndefTag.connect();
                ndefTag.writeNdefMessage(msg);
                ndefTag.close();
            }
            app.appendLog("Terminando proceso de escritura de fecha");
            Toast.makeText(getActivity(), "Se ha inicializado correctamente.", Toast.LENGTH_SHORT).show();
            isNFCActive = false;
            MenuItem item = mOptionsMenu.getItem(0);
            switchMode(item, isNFCActive);
        }
    }

    void execTask(final NFCTask task){
        task.setOnPostExecuteListener(new OnPostExecuteListener<NFCData>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onPostExecute(NFCData response, List<String> errorMessages) {
//                        nfcDataAdapter.add(response);
                if(!errorMessages.isEmpty()){
                    app.appendLog("Errores OnPostExecute:");
                    for (String s : errorMessages){
                        app.appendLog(s);
                    }
                    Toast.makeText(getActivity(), errorMessages.get(0),Toast.LENGTH_SHORT).show();
                } else {
                    dataAdapter.add(response);
                    dataAdapter.notifyDataSetChanged();
                    app.appendLog("Terminando proceso de lectura");
//                    nfcPostExec();
                    nfcAdapter.disableForegroundDispatch(getActivity());
                    nfcAdapter.enableForegroundDispatch(getActivity(), pendingIntent,null, null);
                }
            }
        });
        task.execute();
    }

    void nfcPostExec(){
        try {
            byte[] empty = new byte[0];
            byte[] id = getIntent().getByteArrayExtra(NfcAdapter.EXTRA_ID);
            Tag tag = (Tag) getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
            dumpTagData(tag);
//            String str = "5402656E5CFFA90B4A8E2908F102032051010F5402656E5CFFA90D4A9C2908F903011F51010F5402656E5CFFA90F4A932908FD02042851010F5402656E5CFFA9114A9F2909080105ED51010F5402656E5CFFA9134A972909150106D451010F5402656E5CFFA9154AA129091D0107CF51010F5402656E5CFFA9174A982909250108B951010F5402656E5CFFA9194AA329092C0109A851010F5402656E5CFFA91B4A98290930010A9751010F5402656E5CFFA91D4AA4290939019B86";
            String str = "0F";
            byte[] payload = Converter.toAsciiBytes(str);
            NdefRecord ndefRecord = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, empty, payload);
            NdefMessage msg = new NdefMessage(new NdefRecord[]{ndefRecord});
            app.appendLog("Payload: " + str);
            app.appendLog("Payload codificado: " + new String(payload));
            Ndef ndefTag = Ndef.get(tag);
            if (ndefTag == null) {
                // Let's try to format the Tag in NDEF
                NdefFormatable nForm = NdefFormatable.get(tag);
                if (nForm != null) {
                    nForm.connect();
                    nForm.format(msg);
                    nForm.close();
                }
            } else {
                ndefTag.connect();
                ndefTag.writeNdefMessage(new NdefMessage(new NdefRecord(NdefRecord.TNF_EMPTY, null, null, null)));
                ndefTag.close();
            }
            StringBuilder sb = new StringBuilder("Lectura correcta");
            Toast.makeText(getActivity(), sb.toString(), Toast.LENGTH_SHORT).show();
        } catch (IOException | FormatException e) {
            StringBuilder msg = new StringBuilder(e.getClass().getSimpleName());
            msg.append(": ");
            msg.append(e.getMessage());
            Toast.makeText(getActivity(), msg.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    NFCActivity getActivity(){
        return this;
    }

    private void addProductList(NFCData data) {
        String sensorId = data.getCallsign();
        List<PWPXE> productNames = PWPXE.find(PWPXE.class, "sensor_code=?", sensorId);
        Shipment shipment;
        List<AOSProduct> products;
        if (productNames.size() > 0) {
            List<AOSProduct> tempList = new ArrayList<>();
            for (PWPXE productName : productNames) {
                products = AOSProduct.find(AOSProduct.class, "entity_id=?", productName.getProductId());
                if (products.size() > 0) {
                    tempList.addAll(products);
                }
                try {
                    List<Shipment> shipments = Shipment.find(Shipment.class, "shipment_code=?", productName.getShipmentCode());
                    if(shipments.size() == 1){
                        data.setShipment(shipments.get(0));
                    }
                } finally {}
            }
            data.setProductList(tempList);
        }
    }

    private void deleteData(NFCData data){
        String sensorId = data.getCallsign();
        List<PWPXE> productNames = PWPXE.find(PWPXE.class, "sensor_code=?", sensorId);
        if(productNames.size() > 0){
            for (PWPXE productName: productNames) {
                AOSProduct.deleteAll(AOSProduct.class, "entity_id=?", productName.getProductId());
                Shipment.deleteAll(Shipment.class, "shipment_code=?", productName.getShipmentCode());
                productName.delete();
            }
        }
        data.delete();
    }
}

