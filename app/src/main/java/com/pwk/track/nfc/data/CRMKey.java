package com.pwk.track.nfc.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pwk04 on 05-28-19.
 */

public class CRMKey {

    @Expose
    @SerializedName("grant_type")
    private String GRANT_TYPE;

    @Expose
    @SerializedName("client_id")
    private String CLIENT_ID;

    @Expose
    @SerializedName("client_secret")
    private String CLIENT_SECRET;

    @Expose
    @SerializedName("scope")
    private String SCOPE;

    public CRMKey(){
        GRANT_TYPE = "client_credentials";
        CLIENT_ID = "22001eba-2817-aee1-9ce5-5d03488031e9";
        CLIENT_SECRET = "apkbtt123*";
        SCOPE = "";
    }

    public String getGrantType() {
        return GRANT_TYPE;
    }

    public String getClientId() {
        return CLIENT_ID;
    }

    public String getClientSecret() {
        return CLIENT_SECRET;
    }

    public String getScope() {
        return SCOPE;
    }
}
