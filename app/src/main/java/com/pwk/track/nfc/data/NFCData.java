package com.pwk.track.nfc.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by pwk04 on 05-16-19.
 * @author Carlos Delgadillo
 */

public class NFCData extends SugarRecord<NFCData> {

    @Expose
    @SerializedName("icao24")
    private String icao24;
    @Expose
    @SerializedName("callsign")
    private String callsign;
    @Expose
    @SerializedName("startTime")
    private long startTime;
    @Expose
    @SerializedName("endTime")
    private long endTime;
    @Expose
    @SerializedName("path")
    private JsonArray path;

    private String shipCode;
    private Date captureDate;
    private Date deathDate;
    private float captureWeight;
    private float packageWeight;
    private long MIN_TIME = 1546300800000l;
    private long MAX_TIME = 1558828800000l;
    private long INTERVAL = 900000;
    private String noRef;
    private String product = "ATUN ROJO BALFEGO";
    private String productId;
    private List<AOSProduct> productList;
    private Shipment shipment;
    private boolean sent;

    private double xMin = 0, xMax = 0, yMin = 0, yMax = 0;
    private double[] bounds = new double[4];
    List<NFCPathRecord> records;

    public NFCData(){

    }

    public String getIcao24() {
        return icao24;
    }

    public void setIcao24(String icao24) {
        this.icao24 = icao24;
    }

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    public String getStartTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
        Date date = new Date();
        date.setTime(startTime);
        return sdf.format(date);
    }

    private long setLong(long max, long min){
        long x = min + (long) (Math.random() * (max - min));
        return x;
    }

    private float setFloat(float max, float min){
        float x = min + (float) (Math.random() * (max - min));
        return x;
    }

    private String setRef(){
        StringBuilder sb = new StringBuilder();
        for(int u = 1; u<=7; u++){
            sb.append(1+(int)(Math.random()*(9-0)));
        }
        return sb.toString();
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
        Date deathDate = new Date();
        deathDate.setTime(setLong(startTime,(startTime-(86400*3*1000))));
        this.deathDate = deathDate;
        Date captureDate = new Date();
        captureDate.setTime(setLong(this.deathDate.getTime(),(this.deathDate.getTime()-(86400*2*1000))));
        this.captureDate = captureDate;
        this.captureWeight = setFloat(900,400);
        this.packageWeight = setFloat(this.getCaptureWeight(), (this.getCaptureWeight() -100));
        this.noRef = setRef();

    }

    public String getEndTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
        Date date = new Date();
        date.setTime(endTime);
        return sdf.format(date);
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public JsonArray getPath() {
        return path;
    }

    public void setPath(JsonArray path) {
        this.path = path;
    }

    public List<NFCPathRecord> getRecords(){
        if(records == null)
            records = new ArrayList<>();
        return records;
    }

    public String getCaptureDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        Date date = new Date();
        date.setTime(captureDate.getTime());
        return sdf.format(date);
    }

    public String getDeathDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        Date date = new Date();
        date.setTime(deathDate.getTime());
        return sdf.format(date);
    }

    public float getCaptureWeight() {
        return captureWeight;
    }

    public float getPackageWeight() {
        return packageWeight;
    }

    public String getNoRef() {
        return noRef;
    }

    public String getProduct() {
        return product;
    }

    public String getShipCode() {
       return shipCode;
    }
    
    public CRMNote parseToCRM(){
        CRMNote note = new CRMNote();
        JsonArray data = new JsonArray();
        JsonObject envio = new JsonObject();
        JsonArray devices = new JsonArray();
        for(NFCPathRecord record : getRecords()){
            JsonObject element = new JsonObject();
            element.addProperty("captureWeight", getCaptureWeight());
            element.addProperty("captureDate", captureDate.getTime());
            element.addProperty("boxingWeight", getPackageWeight());
            element.addProperty("boxingDate", deathDate.getTime());
            element.addProperty("latitude", record.getLatitude());
            element.addProperty("longitude", record.getLongitude());
            element.addProperty("altitude", record.getAltitude());
            element.addProperty("temperature", record.getTemp());
            element.addProperty("time", record.getTime());
            data.add(element);
        }
        JsonObject device = new JsonObject();
        device.addProperty("code",getCallsign());
        device.add("data", data);
        devices.add(device);
//        envio.addProperty("code", getShipCode());
        envio.add("devices",devices);
        JsonObject description = new JsonObject();
        description.addProperty("description",envio.toString());
        note.setAttributes(description);
        return note;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public List<AOSProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<AOSProduct> productList) {
        this.productList = productList;
    }

    public void setShipCode(String shipCode) {
        this.shipCode = shipCode;
    }

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }
}
