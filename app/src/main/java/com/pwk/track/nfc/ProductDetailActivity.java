package com.pwk.track.nfc;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.pwk.track.nfc.adapter.AOSProductAdapter;
import com.pwk.track.nfc.data.AOSProduct;

import java.util.List;

public class ProductDetailActivity extends AppCompatActivity {

    private List<AOSProduct> products;
    private App app;
    private RecyclerView recycler;
    private AOSProductAdapter dataAdapter;
    private RecyclerView.LayoutManager lManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        app = (App) getApplicationContext();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Detalle de productos");
        TextView envio, sensor;
        envio = (TextView) findViewById(R.id.envio);
        sensor = (TextView) findViewById(R.id.sensor);
        envio.setText("Envío: " + app.getCurrent().getShipCode());
        sensor.setText("Sensor: " + app.getCurrent().getCallsign());
        recycler = (RecyclerView) findViewById(R.id.recycler2);
        lManager = new LinearLayoutManager(this);
        dataAdapter = new AOSProductAdapter(ProductDetailActivity.this, app.getCurrent().getProductList());
        dataAdapter.setHasStableIds(true);
        recycler.setAdapter(dataAdapter);
        recycler.setLayoutManager(lManager);

//        recycler.setHasFixedSize(true);
        recycler.setItemViewCacheSize(6);

    }
}
