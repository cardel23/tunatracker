package com.pwk.track.nfc;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.pwk.track.nfc.api.CRMTask;
import com.pwk.track.nfc.api.OnPostExecuteListener;
import com.pwk.track.nfc.data.NFCData;
import com.pwk.track.nfc.data.NFCPathRecord;
import com.pwk.track.nfc.format.NFCValueFormatter;


import org.mapsforge.map.android.rendertheme.AssetsRenderTheme;
import org.mapsforge.map.rendertheme.XmlRenderTheme;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.mapsforge.MapsForgeTileProvider;
import org.osmdroid.mapsforge.MapsForgeTileSource;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.util.SimpleRegisterReceiver;
import org.osmdroid.tileprovider.util.StorageUtils;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.infowindow.BasicInfoWindow;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Carlos Delgadillo
 */
public class InfoActivity extends DemoBase implements MapEventsReceiver {

    private LineChart mChart;
    private XAxis xAxis;
    private MapView myOpenMapView;
    private MapController myMapController;
    App app;
    private ItemizedOverlayWithFocus<OverlayItem> capa;
    private static final String SERIES_TITLE = "Temperatura (ºC)/Hora";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        app = (App) getApplicationContext();
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        mChart = findViewById(R.id.chart);
        mChart.setTouchEnabled(true);
        mChart.setPinchZoom(true);
        MyMarkerView mv = new MyMarkerView(getApplicationContext(), R.layout.custom_marker_view);
        mv.setChartView(mChart);
        mChart.setMarker(mv);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CRMTask task = new CRMTask(InfoActivity.this, null, app.getCurrent().parseToCRM());
                task.setOnPostExecuteListener(new OnPostExecuteListener<List<String>>() {
                    @Override
                    public void onPostExecute(List<String> response, List<String> errorMessages) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(InfoActivity.this);

                        if(!errorMessages.isEmpty()){
                            String[] errors = errorMessages.toArray(new String[errorMessages.size()]);
                            alertDialogBuilder.setTitle("Error")
                                    .setItems(errors, null)
                                    .setCancelable(true)
                                    .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                        } else{
                            String[] messages = response.toArray(new String[response.size()]);
                            alertDialogBuilder.setCancelable(false)
                                    .setTitle("Éxito");
                            alertDialogBuilder.setItems(messages,null);
                            alertDialogBuilder.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    app.getCurrent().setSent(true);
                                    dialog.dismiss();
                                    finish();
                                }
                            });

                        }
                        alertDialogBuilder.create().show();
                    }
                });
                task.execute();
            }
        });
        myOpenMapView = (MapView) findViewById(R.id.openmapview);
        try {
//            renderPlot();
            renderData();
        } catch (Exception e){
            Toast.makeText(this, "Error al cargar el gráfico",Toast.LENGTH_SHORT).show();
        }
        boolean check = checkPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,2);
        try {
            renderMap(check);
        } catch (Exception e){
            Toast.makeText(this, "Error al cargar el mapa",Toast.LENGTH_SHORT).show();
        }
    }

    public void renderData() {
        xAxis = mChart.getXAxis();
        xAxis.setDrawLimitLinesBehindData(true);
        xAxis.setLabelRotationAngle(-45);
        xAxis.setValueFormatter(new NFCValueFormatter(app));
        LimitLine ll1 = new LimitLine(-35, "Max");
        ll1.setLineWidth(1f);
        ll1.enableDashedLine(10f, 10f, 0f);
        ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        ll1.setTextSize(10f);

        LimitLine ll2 = new LimitLine(-45f, "Min");
        ll2.setLineWidth(1f);
        ll2.enableDashedLine(10f, 10f, 0f);
        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(10f);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines();
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawLimitLinesBehindData(false);
//        leftAxis.setAxisMaximum(0f);
//        leftAxis.setAxisMinimum(-60f);
        leftAxis.addLimitLine(ll1);
        leftAxis.addLimitLine(ll2);
        mChart.getAxisRight().setEnabled(false);
        setData();
    }

    private void setData() {
        ArrayList<Entry> values = new ArrayList<>();
        NFCData current = app.getCurrent();
        if(current.getRecords().isEmpty()){
            current.getRecords().addAll(NFCPathRecord.find(NFCPathRecord.class,"data_id=?",new String[]{String.valueOf(current.getId())},"","Id asc",""));
        }
        float max = 0, min = 0;
        for (int i=0; i<current.getRecords().size(); i++){
            NFCPathRecord record = current.getRecords().get(i);
            if(i == max && i == min){
                max += record.getTemp();
                min += record.getTemp();
            } else {
                if(record.getTemp() > max)
                    max = (float) record.getTemp();
                else if(record.getTemp() < min)
                    min = (float) record.getTemp();
            }
            values.add(new Entry(i,(float) record.getTemp(),record));
        }
        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setAxisMaximum(max + 7);
        leftAxis.setAxisMinimum(min - 7);
        LineDataSet set1;
        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            set1 = new LineDataSet(values, SERIES_TITLE);
            set1.setDrawIcons(false);
            set1.setLineWidth(0.5f);
            set1.setCircleRadius(1f);
            set1.setDrawCircles(false);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(0f);
            set1.setFormLineWidth(1f);
            set1.setFormSize(15.f);
            set1.setColor(getResources().getColor(R.color.colorPrimaryDark));
//            set1.setHighLightColor(getResources().getColor(R.color.colorPrimaryDark));
            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_blue);
                set1.setFillDrawable(drawable);
            }
            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);
            LineData data = new LineData(dataSets);
            mChart.setData(data);
            mChart.getDescription().setEnabled(false);
        }
}

    private void renderMap(boolean render){
        if(!render){
            myOpenMapView.setVisibility(View.GONE);
        } else {
            Configuration.getInstance().setUserAgentValue("com.pwk.track.nfc(osmdroid)");
            myOpenMapView.setVisibility(View.VISIBLE);
            myOpenMapView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            myOpenMapView.setClickable(true);
            MapEventsOverlay mapEventsOverlay = new MapEventsOverlay(this, this);
            myOpenMapView.getOverlays().add(0, mapEventsOverlay);

            Set<File> mapFiles = findMapFiles();
            if (!mapFiles.isEmpty()) {
                File[] maps = (File[]) mapFiles.toArray();  //TODO scan/prompt for map files (.map)

                XmlRenderTheme theme = null; //null is ok here, uses the default rendering theme if it's not set
                try {
                    theme = new AssetsRenderTheme(getApplicationContext(), "renderthemes/", "rendertheme-v4.xml");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                MapsForgeTileSource fromFiles = MapsForgeTileSource.createFromFiles(maps, theme, "rendertheme-v4");
                MapsForgeTileProvider forge = new MapsForgeTileProvider(
                        new SimpleRegisterReceiver(getApplicationContext()),
                        fromFiles, null);
                myOpenMapView.setTileProvider(forge);
            } else {
                myOpenMapView.setTileSource(TileSourceFactory.MAPNIK);
            }

            myOpenMapView.setBuiltInZoomControls(true);
            myMapController = (MapController) myOpenMapView.getController();
            myMapController.setZoom(16);
            myOpenMapView.setMultiTouchControls(true);

            ArrayList<OverlayItem> puntos = new ArrayList<OverlayItem>();
            NFCData current = app.getCurrent();
            for (int i = 0; i < current.getRecords().size(); i++) {
                NFCPathRecord record = current.getRecords().get(i);
                GeoPoint point = new GeoPoint(record.getLatitude(), record.getLongitude(), record.getAltitude());
                fillSpots(record,point);
                if (i == 0) {
                    myMapController.setCenter(point);
                }
            }
        }
    }

    protected static Set<File> findMapFiles() {
        Set<File> maps = new HashSet<>();
        List<StorageUtils.StorageInfo> storageList =
                StorageUtils.getStorageList();
        for (int i = 0; i < storageList.size(); i++) {
            File f = new File(storageList.get(i).path + File.separator +
                    "osmdroid" + File.separator);
            if (f.exists()) {
                maps.addAll(scan(f));
            }
        }
        return maps;
    }

    static private Collection<? extends File> scan(File f) {
        List<File> ret = new ArrayList<>();
        File[] files = f.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if (pathname.getName().toLowerCase().endsWith(".map"))
                    return true;
                return false;
            }
        });
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                ret.add(files[i]);
            }
        }
        return ret;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.detail:
                intent = new Intent(InfoActivity.this, TrackActivity.class);
                startActivity(intent);
                break;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 2: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    renderMap(true);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public boolean singleTapConfirmedHelper(GeoPoint p) {
        return false;
    }

    @Override
    public boolean longPressHelper(GeoPoint p) {
        return false;
    }

    void fillSpots(NFCPathRecord record, GeoPoint geoPoint){
        Marker startMarker = new Marker(myOpenMapView);
        startMarker.setPosition(geoPoint);
        startMarker.setIcon(getResources().getDrawable(R.drawable.ic_place_36dp));
        startMarker.setAnchor(Marker.ANCHOR_CENTER, 1.0f);
        InfoWindow infoWindow = new BasicInfoWindow(R.layout.bonuspack_bubble, myOpenMapView);
        startMarker.setInfoWindow(infoWindow);
        startMarker.setTitle(record.getStringDate());
        StringBuilder sb = new StringBuilder();
        sb.append("Latitud: " + record.getLatitude() + "N");
        sb.append("\n");
        sb.append("Longitud: " + record.getLongitude() + "E");
        sb.append("\n");
        sb.append("Altitud: " + record.getAltitude() + "m");
        sb.append("\n");
        sb.append("Temperatura: " + record.getTemp() + "ºC");
        sb.append("\n");
        startMarker.setSubDescription(sb.toString());

        myOpenMapView.getOverlays().add(startMarker);

    }
}
