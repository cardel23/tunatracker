package com.pwk.track.nfc.adapter;

import android.content.Context;;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pwk.track.nfc.App;
import com.pwk.track.nfc.R;
import com.pwk.track.nfc.data.AOSProduct;
import java.util.List;

public class AOSProductAdapter extends RecyclerView.Adapter<AOSProductAdapter.ProductViewHolder> {

    private SparseBooleanArray mSelectedItemsIds;
    List<AOSProduct> aosProductList;
    App context;


    public AOSProductAdapter(Context context, List<AOSProduct> products) {
        this.aosProductList = products;
        this.context = (App)context.getApplicationContext();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        public TextView producto, ref, part, maincode;
        public ImageView imagen, productoImagen;

        public ProductViewHolder(@NonNull View v) {
            super(v);
            producto = (TextView) v.findViewById(R.id.producto);
            ref = (TextView) v.findViewById(R.id.ref);
            imagen = (ImageView) v.findViewById(R.id.image);
            productoImagen = (ImageView) v.findViewById(R.id.profile_image);
            part = (TextView) v.findViewById(R.id.part);
        }
    }

    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_product, viewGroup, false);
        return new ProductViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder viewHolder, int i) {
        AOSProduct product = aosProductList.get(i);
        viewHolder.producto.setText("Producto: "+product.getName());
        viewHolder.ref.setText("Parte nº: "+product.getPartNumber());
        viewHolder.part.setText("Código: "+product.getMainCode());
        try{
            if(product.getImageBufferedString() != null) {
                viewHolder.imagen.setImageBitmap(context.loadImageFromInternalStorage(product.getImageBufferedString()));

            } else {
                viewHolder.imagen.setVisibility(View.GONE);
            }
        } catch (Exception e){
            viewHolder.imagen.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return aosProductList.size();
    }

    @Override
    public long getItemId(int position) {
        return aosProductList.get(position).getId();
    }
}
