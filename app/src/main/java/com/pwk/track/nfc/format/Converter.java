package com.pwk.track.nfc.format;

import android.annotation.SuppressLint;

import com.pwk.track.nfc.App;
import com.pwk.track.nfc.data.NFCPathRecord;
import com.pwk.track.nfc.exception.ConvertException;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by pwk04 on 06-07-19.
 */

public class Converter {

    public static HashMap<Float, String> map;
    public static final int LAT = 0, LON = 1;

    public static String convertMessage(String inputString) throws Exception {
        StringBuilder sb = new StringBuilder();
        String sub = null;
//		sub = inputString.substring(0, 8);
        int startPosition = 50; //Desde donde empiezan las tramas segun documentacion
        boolean isEOF = false;
        while(!isEOF) {
            sb.append(getConvertedDate(inputString.substring(startPosition+0, startPosition+8))+"\n");
            sb.append(getConvertedTemp(inputString.substring(startPosition+8,startPosition+12))+"\n");
            sb.append(dms2dec(inputString.substring(startPosition+12, startPosition+14), inputString.substring(startPosition+14, startPosition+18), Converter.LAT)+"\n");
            sb.append(dms2dec(inputString.substring(startPosition+18, startPosition+20), inputString.substring(startPosition+20, startPosition+24), Converter.LON)+"\n");
            if(inputString.substring(startPosition+24,startPosition+26).equals("FE"))
                isEOF=true;
            else
                startPosition += 38;
//			System.out.println(sb.toString());
        }
        return sb.toString();
    }

    public static List<NFCPathRecord> getConvertedRecords(List<ExtendedNdefRecord> records) throws ConvertException, UnsupportedEncodingException {
        List<NFCPathRecord> pathRecords = new ArrayList<>();
        for(ExtendedNdefRecord record : records){
            String inputString = Converter.asciiToHex(record.str());
            int startPosition = 8; //Desde donde empiezan las tramas segun documentacion
            boolean isEOF = false;
            App.getContext().appendLog("Input: "+inputString);
            int reg = 1;
            while(!isEOF) {
                App.getContext().appendLog("Registro "+reg+" posición "+startPosition);
                try {
                    NFCPathRecord nfcPathRecord = new NFCPathRecord();
                    nfcPathRecord.setTime(getConvertedDate(inputString.substring(startPosition+0, startPosition+8)));
                    nfcPathRecord.setTemp(getConvertedTemp(inputString.substring(startPosition+8,startPosition+12)));
                    nfcPathRecord.setLatitude(dms2dec(inputString.substring(startPosition+12, startPosition+14), inputString.substring(startPosition+14, startPosition+18), Converter.LAT));
                    nfcPathRecord.setLongitude(dms2dec(inputString.substring(startPosition+18, startPosition+20), inputString.substring(startPosition+20, startPosition+24), Converter.LON));
                    if(nfcPathRecord.getId()==null)
                        nfcPathRecord.save();
                    pathRecords.add(nfcPathRecord);
                    if(inputString.length() == startPosition+24)
                        isEOF=true;
                    else
                        startPosition += 38;
                } catch (ConvertException e) {
                    throw new ConvertException(e.getMessage());
                }
                reg++;
            }
        }
        return pathRecords;
    }

    public String toHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = bytes.length - 1; i >= 0; --i) {
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append('0');
            sb.append(Integer.toHexString(b));
            if (i > 0) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }
    private static long getConvertedDate(String string) {
        App.getContext().appendLog("Fecha input: "+string);
        long convertedDate = Long.parseLong(string, 16)*1000;
        App.getContext().appendLog("Fecha unix: "+ String.valueOf(convertedDate));
        return convertedDate;
    }

    private static float getConvertedTemp(String str){
        App.getContext().appendLog("Temp input: "+str);
        try {
            String binStr = Integer.toBinaryString(Integer.parseInt(str, 16));
            while (binStr.length() < 16) {
                binStr = "0" + binStr;
            }
            App.getContext().appendLog("Bin temp: "+ binStr);
            return parseTemp(binStr.substring(1, 10), binStr.substring(10, 16));
        } catch (ConvertException e){
            return -274;
        }
    }

    private static float parseTemp(String str1, String str2) throws ConvertException {
        int i1 = Integer.parseInt(str1, 2);
        float f2 = getTempDecimals(str2);
        return (float) ((i1 + f2) - 273.15);
    }

    private static float getTempDecimals(String str) throws ConvertException {
        if(str.length()<6) {
            throw new ConvertException("Cadena de tamaño incorrecto");
        }
        float sum = 0f;
        if(str.charAt(0) == '1') sum += 0.5f;
        if(str.charAt(1) == '1') sum += 0.25f;
        if(str.charAt(2) == '1') sum += 0.13f;
        if(str.charAt(3) == '1') sum += 0.06f;
        if(str.charAt(4) == '1') sum += 0.03f;
        if(str.charAt(5) == '1') sum += 0.02f;
        return sum;

    }

    private static float dms2dec(String str1, String str2, int type) throws ConvertException {
        switch (type){
            case LAT:
                int i1 = Integer.parseInt(str1,16);
//                i1 = (i1>90?(i1>=96?95-i1:throw new Exception("");):i1);
                if(i1>90){
                    if(i1>=128 && i1<218)
                        i1 = 128-i1;
                    else
                        throw new ConvertException("Valor de grado inválido ["+str1+"]");
                }
                if(i1==90)
                    return 90f;
                int t = Integer.parseInt(str2, 16);
                if(t>5999){
                    throw new ConvertException("Valor hexadecimal de minutos "+str2+" superior al máximo [176F]");
                }
                String strValue = String.valueOf(t);
                int i2, i3;
                try {
                    i2 = Integer.parseInt(strValue.substring(0, 2));
                } catch (NumberFormatException e){i2 = 0;}
                try {
                    i3 = Integer.parseInt(strValue.substring(2));
                } catch (NumberFormatException e){i3 = 0;}
                return (float) i1+(i2/60f)+(i3/3600f);
            case LON:
                int l1 = Integer.parseInt(str1, 16);
                if(l1>180){
                    throw new ConvertException("Valor hexadecimal de grados "+str1+" superior al máximo [B4]");
                }
                if(l1==180)
                    return 180f;
                int sign = 1;
                int check = Integer.parseInt((str2.substring(0,1)));
                if(check > 1){
                    sign = sign*-1;
                    if(check < 8)
                        throw new ConvertException("Valor de minutos inválido ["+str2+"]");
                    else
                        str2 = String.valueOf(check - 8).concat(str2.substring(1,3));
                }
                String strCheck = String.valueOf(Integer.parseInt(str2, 16));
                while (strCheck.length()<4){
                    strCheck = "0" + strCheck;
                }
                int l2,l3;
                try {
                    l2 = Integer.parseInt(strCheck.substring(0, 2));
                } catch (NumberFormatException e){l2 = 0;}
                try {
                    l3 = Integer.parseInt(strCheck.substring(2));
                } catch (NumberFormatException e){l3 = 0;}
                return (float) sign * l1+(l2/60f)+(l3/3600f);
            default:
                throw new ConvertException("Valor desconocido");
        }
    }

    private static String convertDate(long timestamp) {
        return Long.toHexString(timestamp);
    }

    private static String convertTemp(double temp) {
        temp += 273.15;
        int i = Integer.parseInt(
                binTempDecimals(temp), 2);
        return Integer.toString(i, 16);
    }

    private static String convertGPS(double lat, double lon) throws ConvertException {
        String hexStr = dec2dms(lat, LAT).concat(dec2dms(lon, LON));
        return hexStr;
    }

    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(x);
        return buffer.array();
    }

    public static byte[] longToHex(final long l) {
        long v = l & 0xFFFFFFFFFFFFFFFFL;
        byte[] result = new byte[16];
        Arrays.fill(result, 0, result.length, (byte) 0);
        for (int i = 0; i < result.length; i += 2) {
            byte b = (byte) ((v & 0xFF00000000000000L) >> 56);
            byte b2 = (byte) (b & 0x0F);
            byte b1 = (byte) ((b >> 4) & 0x0F);
            if (b1 > 9)
                b1 += 39;
            b1 += 48;
            if (b2 > 9)
                b2 += 39;
            b2 += 48;
            result[i] = (byte) (b1 & 0xFF);
            result[i + 1] = (byte) (b2 & 0xFF);
            v <<= 8;
        }
        return result;
    }

    public static byte[] toAsciiBytes(String hex) throws UnsupportedEncodingException {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < hex.length(); i = i + 2) {
            String s = hex.substring(i, i + 2);
            int n = Integer.valueOf(s, 16);
            builder.append((char) n);
        }
        return builder.toString().getBytes("ISO-8859-1");
        // return builder.toString().getBytes();
    }

    public static String asciiToHex(String asciiStr) {
        char[] chars = asciiStr.toCharArray();
        StringBuilder hex = new StringBuilder();
        for (char ch : chars) {
            int i = (int) ch;
            if (i < 16)
                hex.append("0");
            hex.append(Integer.toHexString(i).toUpperCase());
        }
        return hex.toString();
    }



    public static String binTempDecimals(double temp) {
        int units = (int) temp;
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        float decimals = Float.valueOf(decimalFormat.format(temp - units));
        // float decimals = Math.round(temp-units);
        if(decimals<0.1){
            decimals += 0.1;
        }
        String binUnits = Integer.toBinaryString(units);
        String binDec = "";
        while (binDec == "") {
            if (decMap().containsKey(decimals)) {
                binDec = decMap().get(decimals);
            } else {
                if (decimals < 0.02)
                    binDec = "000000";
                else
                    decimals = Float.valueOf(decimalFormat.format(decimals - 0.01));
            }
        }
        return binUnits.concat(binDec);
    }

    private static String  dec2dms(double val, int type) throws ConvertException {
        StringBuilder gpsStr = new StringBuilder();
        int deg = (int) val;
        double min = (Math.abs(val) - Math.abs(deg)) * 60;
        int res = (int) min;
        double sec = (min - res) * 60;
        String minSec = String.valueOf(res).concat(String.valueOf((int)sec));
        int ms = Integer.parseInt(minSec);
        switch (type) {
            case LAT:
                if (deg < 0) {
                    deg = 128 + (deg * -1);
                    if (deg < 128 | deg > 218)
                        throw new ConvertException("Valor fuera de rango");
                }
                String check = Integer.toHexString(deg);
                while (check.length() < 2)
                    check = "0".concat(check);
                gpsStr.append(check);
                if (ms > 5999) {
                    throw new ConvertException("Valor fuera de rango");
                } else {
                    check = Integer.toHexString(ms);
                    while (check.length() < 4)
                        check = "0".concat(check);
                    gpsStr.append(check);
                }
                break;
            case LON:
                if (ms > 5999) {
                    throw new ConvertException("Valor fuera de rango");
                }
                if (deg > 180) {
                    throw new ConvertException("Valor hexadecimal de grados " + deg + " superior al máximo 180");
                }
                if (deg < 0 & deg >= -180) {
                    deg *= -1;
                    ms += 32768;
                }
                String str = Integer.toHexString(deg);
                while (str.length() < 2)
                    str = "0".concat(str);
                gpsStr.append(str);
                str = Integer.toHexString(ms);
                while (str.length() < 4)
                    str = "0".concat(str);
                gpsStr.append(str);
                break;

        }
        return gpsStr.toString();
    }

    private static HashMap<Float, String> decMap() {
        if (map == null) {
            map = new HashMap<>();
            map.put(0.02f, "000001");
            map.put(0.03f, "000010");
            map.put(0.05f, "000011");
            map.put(0.06f, "000100");
            map.put(0.08f, "000101");
            map.put(0.09f, "000110");
            map.put(0.11f, "000111");
            map.put(0.13f, "001000");
            map.put(0.15f, "001001");
            map.put(0.16f, "001010");
            map.put(0.19f, "001100");
            map.put(0.21f, "001101");
            map.put(0.22f, "001110");
            map.put(0.24f, "001111");
            map.put(0.25f, "010000");
            map.put(0.27f, "010001");
            map.put(0.28f, "010010");
            map.put(0.3f, "010011");
            map.put(0.31f, "010100");
            map.put(0.33f, "010101");
            map.put(0.34f, "010110");
            map.put(0.36f, "010111");
            map.put(0.38f, "011000");
            map.put(0.4f, "011001");
            map.put(0.41f, "011010");
            map.put(0.43f, "011011");
            map.put(0.44f, "011100");
            map.put(0.46f, "011101");
            map.put(0.47f, "011110");
            map.put(0.49f, "011111");
            map.put(0.5f, "100000");
            map.put(0.52f, "100001");
            map.put(0.53f, "100010");
            map.put(0.55f, "100011");
            map.put(0.56f, "100100");
            map.put(0.58f, "100101");
            map.put(0.61f, "100111");
            map.put(0.63f, "101000");
            map.put(0.65f, "101001");
            map.put(0.66f, "101010");
            map.put(0.68f, "101011");
            map.put(0.69f, "101100");
            map.put(0.71f, "101101");
            map.put(0.72f, "101110");
            map.put(0.74f, "101111");
            map.put(0.75f, "110000");
            map.put(0.77f, "110001");
            map.put(0.78f, "110010");
            map.put(0.8f, "110011");
            map.put(0.81f, "110100");
            map.put(0.83f, "110101");
            map.put(0.84f, "110110");
            map.put(0.86f, "110111");
            map.put(0.88f, "111000");
            map.put(0.9f, "111001");
            map.put(0.91f, "111010");
            map.put(0.93f, "111011");
            map.put(0.94f, "111100");
            map.put(0.96f, "111101");
            map.put(0.97f, "111110");
            map.put(0.99f, "111111");
        }
        return map;
    }

    public static String fakeRecords() throws ConvertException {
        ArrayList<FakeRecord> a = new ArrayList<>();
        long time = System.currentTimeMillis()/1000;
        switch (getRandom(0, 4)){
            case 0:
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.269327, 2.167655));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.236761, 2.274573));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.172485, 2.483010));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.110168, 2.682136));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.048580, 2.883662));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.957122, 3.158998));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.847344, 3.472446));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 39.971483, 5.600456));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 38.927851, 7.495909));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 37.841284, 9.052136));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 36.854858, 10.219602));
                break;
            case 1:
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.355371, 0.465546));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.099955, -2.775448));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.451341, -5.938392));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.532177, -13.818908));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.290008, -22.669645));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.306995, -26.110016));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.480390, -35.863314));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.040246, -46.807794));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.589943, -55.339317));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.428715, -70.706642));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.691619, -74.168999));
                break;
            case 2:
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.247358, 2.240382));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.213562, 2.349801));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.135516, 2.604889));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.084553, 2.769665));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.025786, 2.959541));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.888103, 3.356054));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.414232, 4.572661));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 39.534600, 6.470656));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 38.406437, 8.276445));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 37.308685, 9.706187));
                break;
            case 3:
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 41.435367, 0.489998));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.252551, -3.778889));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.328957, -9.104096));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.770212, -17.361312));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.257530, -23.001731));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.441518, -31.606272));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 44.441451, -40.391805));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 43.700477, -52.978817));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 42.982124, -60.539820));time+=900;
                a.add(new FakeRecord(time, getRandom(-25.0, -15.0), 40.754080, -73.901505));
                break;
            case 4:
                return "5402656E5CFFA90B4A8E2908F102032051010F5402656E5CFFA90D4A9C2908F903011F51010F5402656E5CFFA90F4A932908FD02042851010F5402656E5CFFA9114A9F2909080105ED51010F5402656E5CFFA9134A972909150106D451010F5402656E5CFFA9154AA129091D0107CF51010F5402656E5CFFA9174A982909250108B951010F5402656E5CFFA9194AA329092C0109A851010F5402656E5CFFA91B4A98290930010A9751010F5402656E5CFFA91D4AA4290939019B86";
            default:
                return null;


        }
        StringBuilder sb = new StringBuilder();
        int size = a.size();
        int c = 0;
        Iterator<FakeRecord> recordIterator = a.iterator();

        while (recordIterator.hasNext()){
            sb.append("5402656E");
            FakeRecord r = recordIterator.next();
            sb.append(convertDate(r.time));
            sb.append(convertTemp(r.temp));
            sb.append(convertGPS(r.lat, r.lon));
            if(c<=size-2){
                sb.append("51010F");
            }
            c++;
        }

        return sb.toString();
    }

    public static int getRandom(int min, int max) {
        int i = new Random().nextInt(max - min + 1) + min;
        return i;
    }

    @SuppressLint("NewApi")
    public static double getRandom(double min, double max) {
//        DecimalFormat format = new DecimalFormat("#.##");
//        return Double.valueOf(format.format(ThreadLocalRandom.current().nextDouble(min, max)));
        double i = ThreadLocalRandom.current().nextDouble(min, max);
        return i;
    }

}

class FakeRecord{
    public long time;
    public double temp, lat, lon;
    FakeRecord(long time, double temp, double lat, double lon){
        this.time = time;
        this.temp = temp;
        this.lon = lon;
        this.lat = lat;
    }
}
