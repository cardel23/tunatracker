package com.pwk.track.nfc.exception;

/**
 * Created by pwk04 on 06-07-19.
 */

public class ConvertException extends Exception{

    public ConvertException(String message){
        super(message);
    }
}
