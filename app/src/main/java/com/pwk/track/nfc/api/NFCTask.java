package com.pwk.track.nfc.api;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.gson.JsonElement;
import com.pwk.track.nfc.App;
import com.pwk.track.nfc.data.AOSProduct;
import com.pwk.track.nfc.data.CRMKey;
import com.pwk.track.nfc.data.CRMToken;
import com.pwk.track.nfc.data.Constants;
import com.pwk.track.nfc.data.NFCData;
import com.pwk.track.nfc.data.NFCPathRecord;
import com.pwk.track.nfc.data.PWPXE;
import com.pwk.track.nfc.data.Shipment;
import com.pwk.track.nfc.exception.ConvertException;
import com.pwk.track.nfc.format.Converter;
import com.pwk.track.nfc.format.ExtendedNdefRecord;
import com.pwk.track.nfc.utils.BitmapUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;

/**
 * Created by pwk04 on 05-16-19.
 */

public class NFCTask extends TaskWithCallback<Void, Void, NFCData> {

    private long MIN_TIME = 1546300800000l;
    private long MAX_TIME = 1558828800000l;
    private long INTERVAL = 900000;
    private List<ExtendedNdefRecord> records;
    private String tagId;
    public NFCTask(Context context) {
        super(context);
    }
    public NFCTask(Context context, List<ExtendedNdefRecord> records){
        super(context);
        this.records = records;
    }
    public NFCTask(Context context, List<ExtendedNdefRecord> records, String tagId){
        super(context);
        this.records = records;
        this.tagId = tagId;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected NFCData doInBackground(Void... voids) {
        errorMessages = new ArrayList<>();
        try {
            NFCData data;
            try {
                data = NFCData.findWithQuery(NFCData.class, "SELECT * FROM NFC_DATA WHERE callsign=?", tagId).get(0);
                if (data == null) {
                    data = new NFCData();
                }
            }
            catch (IndexOutOfBoundsException e){
                data = new NFCData();
            }
            catch (Exception e){
                throw e;
            }
            buildData(data);
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            if(e.getMessage() != null){
                errorMessages.add(e.getMessage() );
            }else{
                errorMessages.add("ERROR");
            }
        }
        return null;
    }

    private void buildData(NFCData data) throws ConvertException, IOException {

        List<NFCPathRecord> convertedRecords = Converter.getConvertedRecords(records);
        if(data.getId() == null) {
            data.setStartTime(convertedRecords.get(0).getTime());
            data.setCallsign(tagId);
            addProductList(data);
            data.save();
        }
        data.setEndTime(convertedRecords.get(records.size() - 1).getTime());
        for (NFCPathRecord p: convertedRecords) {
            p.setDataId(data.getId());
            p.save();
        }
        data.getRecords().addAll(convertedRecords);
        data.save();
    }

    private void addProductList(NFCData data) throws IOException {
        String sensorId = data.getCallsign();
        List<PWPXE> productNames = PWPXE.find(PWPXE.class, "sensor_code=?", sensorId);
        Shipment shipment;
        List<AOSProduct> products;
        if(productNames.size() > 0){
            List<AOSProduct> tempList = new ArrayList<>();
            for (PWPXE productName: productNames) {
                products = AOSProduct.find(AOSProduct.class, "entity_id=?", productName.getProductId());
                if(products.size()>0){
                    tempList.addAll(products);
                }
                try {
                    shipment = Shipment.find(Shipment.class, "shipment_code=?", productName.getShipmentCode()).get(0);
                    data.setShipment(shipment);
                } catch (IndexOutOfBoundsException e){

                }
            }
            data.setProductList(tempList);
        } else {
            CRMKey key = new CRMKey();
            SuiteCRMService service = ServiceClient.tokenService();
            CRMToken token = service.getAccessToken(key).execute().body();
            queryParams.put("codigo_sensor_c", sensorId);
            JsonElement shipCode = ServiceClient.crmService(token).getLastShipment(queryParams).execute().body();
            queryParams = new HashMap<>();
            queryParams.put("filter[codigo_envio_c][Eq]", shipCode.getAsJsonObject().get("codigo_envio_c").getAsString());
            queryParams.put("filter[operator]", "AND");
            queryParams.put("filter[codigo_sensor_c][eq]", sensorId);
            productNames = ServiceClient.crmService(token).getProdMeeting(queryParams).execute().body();
            products = new ArrayList<>();
            for (PWPXE productName : productNames) {
                if(data.getShipCode()==null)
                    data.setShipCode(productName.getShipmentCode());
                productName.save();
                try {
                    List<AOSProduct> list = ServiceClient.crmService(token).getAOSProducts(productName.getProductId()).execute().body();
                    for(AOSProduct p : list){
                        if(p.getProductImageHtml() != null & !p.getProductImageHtml().equals("")) {
                            BufferedInputStream bis = getImageBuff("http://server3.peoplewalking.com/demo/balfego/tuna-tracker/suitecrm/upload/" + p.getProductImageHtml());
                            Bitmap bm = BitmapFactory.decodeStream(bis);
//                            Bitmap bm = BitmapUtils.loadBitmap("http://server3.peoplewalking.com/demo/balfego/tuna-tracker/suitecrm/upload/" + p.getProductImageHtml());
                            p.setImageBufferedString(app.saveImageToInternalSorage(bm,Constants.PRODUCT_IMAGES_DIRNAME, p.getProductImageHtml()));
                        }
                        p.save();
                    }
                    products.addAll(list);
                } finally {}
                try {
                    queryParams = new HashMap<>();
                    queryParams.put("filter[codigo_envio_c][Eq]", data.getShipCode());
                    shipment = ServiceClient.crmService(token).getShipments(queryParams).execute().body().get(0);
                    shipment.save();
                    data.setShipment(shipment);
                } finally {}
            }
            data.setProductList(products);
        }
    }

    private BufferedInputStream getImageBuff(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request;
        request = new Request.Builder().url(url).get().build();
        ResponseBody in = client.newCall(request).execute().body();
        InputStream inputStream = in.byteStream();
//        Log.i("inputStream","inputstream value = "+inputStream);
        // convert inputstream to bufferinputstream
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        return bufferedInputStream;
    }

    private long getStartTime() {
        long x = MIN_TIME + (long) (Math.random() * (MAX_TIME - MIN_TIME));
        return x;
    }

    @Override
    protected void onPostExecute(NFCData response) {
        // TODO Auto-generated method stub
        super.onPostExecute(response);
    }
}
